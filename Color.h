#pragma once

enum Color_ostream_state
{
	OS_COLOR_RGBA_4FLOAT,
	OS_COLOR_ARGB_4FLOAT,
	OS_COLOR_ARGB_4UINT,
	OS_COLOR_ARGB_UINT_HEXADECIMAL,
	OS_COLOR_RGB_HEXCODE,
	OS_COLOR_RGB_3FLOAT,
	OS_COLOR_CMYK_4FLOAT,
	OS_COLOR_HSL_3FLOAT
};

enum ColorblindFilter
{
	COLORBLIND_NORMAL,
	COLORBLIND_PROTANOPIA,
	COLORBLIND_PROTANOMALY,
	COLORBLIND_DEUTERANOPIA,
	COLORBLIND_DEUTERANOMALY,
	COLORBLIND_TRITANOPIA,
	COLORBLIND_TRITANOMALY,
	COLORBLIND_ACHROMATOPSIA,
	COLORBLIND_ACHROMATOMALY
};

struct RGBColor;
struct RGBAColor;
struct CMYKColor;
struct HSLColor;
struct RGBColorF;
struct RGBAColorF;
struct CMYKColorF;
struct HSLColorF;

struct Color
{
	uint32 m_argb;
	Color( void );
	Color( uint32 argb );
	Color( float64 r, float64 g, float64 b, float64 a = 1.0 );
	Color( float32 r, float32 g, float32 b, float32 a = 1.0f );
	Color( const RGBColor& rhs );
	Color( const RGBAColor& rhs );
	Color( const CMYKColor& rhs );
	Color( const HSLColor& rhs );
	Color( const RGBColorF& rhs );
	Color( const RGBAColorF& rhs );
	Color( const CMYKColorF& rhs );
	Color( const HSLColorF& rhs );
	static const Color
		ZERO, BLACK, WHITE,
		RED, GREEN, BLUE,
		CYAN, MAGENTA, YELLOW,
		ORANGE, PURPLE, PINK,
		HOT_PINK, VIOLET, INDIGO,
		LAVENDER, SKY_BLUE, NAVY_BLUE,
		SILVER, SEA_BLUE, SEA_GREEN,
		SAPPHIRE, SCARLET, BROWN,
		DARK_BROWN, PEACH, PERIDOT,
		ROSE, ROSE_PINK, ROSE_QUARTZ,
		PEARL, PINK_PEARL, PEARL_AQUA,
		TAN, AMBER, GRAY,
		GOLD, DEEP_PINK, RUBY,
		CHERRY, CLASSIC_ROSE, COPPER,
		CRIMSON, CRIMSON_RED, CERULEAN,
		OLIVE, MAROON, AQUAMARINE,
		MEDIUM_AQUAMARINE, MAUVE, TEAL,
		TURQUOISE, LIME, LIME_GREEN,
		LILAC, ELECTRIC_LIME, AZURE,
		CHARTREUSE, LIGHT_GRAY, APRICOT,
		OCHRE, PLUM, DARK_GRAY,
		BLUE_GREEN, BLUE_VIOLET, RED_ORANGE,
		ORANGE_RED, RED_VIOLET, AMETHYST,
		DARK_RED, DARK_GREEN, DARK_BLUE,
		DARK_CYAN, DARK_MAGENTA, LIGHT_GREEN,
		LIGHT_BLUE, LIGHT_CYAN, LIGHT_YELLOW,
		TANGELO, BRONZE, XANADU;
	bool operator==( const Color& ) const;
	bool operator!=( const Color& ) const;
	Color GetWebSafeColor( void ) const;
	Color SimulateColorblind( ColorblindFilter ) const;
};

struct RGBColor
{
	float64 red;
	float64 green;
	float64 blue;
	RGBColor( void );
	RGBColor( const Color& color );
	RGBColor( float64 r, float64 g, float64 b );
	RGBColor( const RGBColorF& rgb );
};

struct RGBAColor
{
	float64 red;
	float64 green;
	float64 blue;
	float64 alpha;
	RGBAColor( void );
	RGBAColor( const Color& color );
	RGBAColor( float64 r, float64 g, float64 b, float64 a = 1.0 );
	RGBAColor( const RGBAColorF& rgba );
};

struct CMYKColor
{
	float64 cyan;
	float64 magenta;
	float64 yellow;
	float64 black;
	CMYKColor( void );
	CMYKColor( const Color& color );
	CMYKColor( float64 c, float64 m, float64 y, float64 k );
	CMYKColor( const CMYKColorF& cmyk );
};

struct HSLColor
{
	float64 hue;
	float64 saturation;
	float64 lightness;
	HSLColor( void );
	HSLColor( const Color& color );
	HSLColor( float64 h, float64 s, float64 l );
	HSLColor( const HSLColorF& hsl );
};

struct RGBColorF
{
	float32 red;
	float32 green;
	float32 blue;
	RGBColorF( void );
	RGBColorF( const Color& color );
	RGBColorF( float32 r, float32 g, float32 b );
	RGBColorF( const RGBColor& rgb );
};

struct RGBAColorF
{
	float32 red;
	float32 green;
	float32 blue;
	float32 alpha;
	RGBAColorF( void );
	RGBAColorF( const Color& color );
	RGBAColorF( float32 r, float32 g, float32 b, float32 a = 1.0f );
	RGBAColorF( const RGBAColor& rgba );
};

struct CMYKColorF
{
	float32 cyan;
	float32 magenta;
	float32 yellow;
	float32 black;
	CMYKColorF( void );
	CMYKColorF( const Color& color );
	CMYKColorF( float32 c, float32 m, float32 y, float32 k );
	CMYKColorF( const CMYKColor& cmyk );
};

struct HSLColorF
{
	float32 hue;
	float32 saturation;
	float32 lightness;
	HSLColorF( void );
	HSLColorF( const Color& color );
	HSLColorF( float32 h, float32 s, float32 l );
	HSLColorF( const HSLColor& hsl );
};

Color BlendColors( const Color& dst, const Color& src );
RGBAColorF BlendColors( const RGBAColorF& dst, const RGBAColorF& src );
RGBAColor BlendColors( const RGBAColor& dst, const RGBAColor& src );

Color SimulateColorblind( const Color&, ColorblindFilter );

void ConvertRGBtoCMYK( float64 r, float64 g, float64 b, float64& c, float64& m, float64& y, float64& k );
void ConvertCMYKtoRGB( float64 c, float64 m, float64 y, float64 k, float64& r, float64& g, float64& b );
void ConvertRGBtoHSL( float64 r, float64 g, float64 b, float64& h, float64& s, float64& l );
void ConvertHSLtoRGB( float64 h, float64 s, float64 l, float64& r, float64& g, float64& b );
void ConvertHSLtoCMYK( float64 h, float64 s, float64 l, float64& c, float64& m, float64& y, float64& k );
void ConvertCMYKtoHSL( float64 c, float64 m, float64 y, float64 k, float64& h, float64& s, float64& l );
void ConvertRGBtoCMYK( float32 r, float32 g, float32 b, float32& c, float32& m, float32& y, float32& k );
void ConvertCMYKtoRGB( float32 c, float32 m, float32 y, float32 k, float32& r, float32& g, float32& b );
void ConvertRGBtoHSL( float32 r, float32 g, float32 b, float32& h, float32& s, float32& l );
void ConvertHSLtoRGB( float32 h, float32 s, float32 l, float32& r, float32& g, float32& b );
void ConvertHSLtoCMYK( float32 h, float32 s, float32 l, float32& c, float32& m, float32& y, float32& k );
void ConvertCMYKtoHSL( float32 c, float32 m, float32 y, float32 k, float32& h, float32& s, float32& l );
void ConvertRGBtoCMYK( float32 r, float32 g, float32 b, float64& c, float64& m, float64& y, float64& k );
void ConvertCMYKtoRGB( float32 c, float32 m, float32 y, float32 k, float64& r, float64& g, float64& b );
void ConvertRGBtoHSL( float32 r, float32 g, float32 b, float64& h, float64& s, float64& l );
void ConvertHSLtoRGB( float32 h, float32 s, float32 l, float64& r, float64& g, float64& b );
void ConvertHSLtoCMYK( float32 h, float32 s, float32 l, float64& c, float64& m, float64& y, float64& k );
void ConvertCMYKtoHSL( float32 c, float32 m, float32 y, float32 k, float64& h, float64& s, float64& l );
void ConvertRGBtoCMYK( float64 r, float64 g, float64 b, float32& c, float32& m, float32& y, float32& k );
void ConvertCMYKtoRGB( float64 c, float64 m, float64 y, float64 k, float32& r, float32& g, float32& b );
void ConvertRGBtoHSL( float64 r, float64 g, float64 b, float32& h, float32& s, float32& l );
void ConvertHSLtoRGB( float64 h, float64 s, float64 l, float32& r, float32& g, float32& b );
void ConvertHSLtoCMYK( float64 h, float64 s, float64 l, float32& c, float32& m, float32& y, float32& k );
void ConvertCMYKtoHSL( float64 c, float64 m, float64 y, float64 k, float32& h, float32& s, float32& l );
void ConvertRGBtoCMYK( const RGBColor& rgb, CMYKColor& cmyk );
void ConvertCMYKtoRGB( const CMYKColor& cmyk, RGBColor& rgb );
void ConvertRGBtoHSL( const RGBColor& rgb, HSLColor& hsl );
void ConvertHSLtoRGB( const HSLColor& hsl, RGBColor& rgb );
void ConvertHSLtoCMYK( const HSLColor& hsl, CMYKColor& cmyk );
void ConvertCMYKtoHSL( const CMYKColor& cmyk, HSLColor& hsl );
void ConvertRGBtoCMYK( const RGBColorF& rgb, CMYKColorF& cmyk );
void ConvertCMYKtoRGB( const CMYKColorF& cmyk, RGBColorF& rgb );
void ConvertRGBtoHSL( const RGBColorF& rgb, HSLColorF& hsl );
void ConvertHSLtoRGB( const HSLColorF& hsl, RGBColorF& rgb );
void ConvertHSLtoCMYK( const HSLColorF& hsl, CMYKColorF& cmyk );
void ConvertCMYKtoHSL( const CMYKColorF& cmyk, HSLColorF& hsl );
void ConvertRGBtoCMYK( const RGBColorF& rgb, CMYKColor& cmyk );
void ConvertCMYKtoRGB( const CMYKColorF& cmyk, RGBColor& rgb );
void ConvertRGBtoHSL( const RGBColorF& rgb, HSLColor& hsl );
void ConvertHSLtoRGB( const HSLColorF& hsl, RGBColor& rgb );
void ConvertHSLtoCMYK( const HSLColorF& hsl, CMYKColor& cmyk );
void ConvertCMYKtoHSL( const CMYKColorF& cmyk, HSLColor& hsl );
void ConvertRGBtoCMYK( const RGBColor& rgb, CMYKColorF& cmyk );
void ConvertCMYKtoRGB( const CMYKColor& cmyk, RGBColorF& rgb );
void ConvertRGBtoHSL( const RGBColor& rgb, HSLColorF& hsl );
void ConvertHSLtoRGB( const HSLColor& hsl, RGBColorF& rgb );
void ConvertHSLtoCMYK( const HSLColor& hsl, CMYKColorF& cmyk );
void ConvertCMYKtoHSL( const CMYKColor& cmyk, HSLColorF& hsl );
void ConvertRGBAtoCMYK( const RGBAColor& rgba, CMYKColor& cmyk );
void ConvertCMYKtoRGBA( const CMYKColor& cmyk, RGBAColor& rgba );
void ConvertRGBAtoHSL( const RGBAColor& rgba, HSLColor& hsl );
void ConvertHSLtoRGBA( const HSLColor& hsl, RGBAColor& rgba );
void ConvertRGBAtoCMYK( const RGBAColorF& rgba, CMYKColorF& cmyk );
void ConvertCMYKtoRGBA( const CMYKColorF& cmyk, RGBAColorF& rgb );
void ConvertRGBAtoHSL( const RGBAColorF& rgba, HSLColorF& hsl );
void ConvertHSLtoRGBA( const HSLColorF& hsl, RGBAColorF& rgba );
void ConvertRGBAtoCMYK( const RGBAColorF& rgba, CMYKColor& cmyk );
void ConvertCMYKtoRGBA( const CMYKColorF& cmyk, RGBAColor& rgba );
void ConvertRGBAtoHSL( const RGBAColorF& rgba, HSLColor& hsl );
void ConvertHSLtoRGBA( const HSLColorF& hsl, RGBAColor& rgba );
void ConvertRGBAtoCMYK( const RGBAColor& rgba, CMYKColorF& cmyk );
void ConvertCMYKtoRGBA( const CMYKColor& cmyk, RGBAColorF& rgba );
void ConvertRGBAtoHSL( const RGBAColor& rgba, HSLColorF& hsl );
void ConvertHSLtoRGBA( const HSLColor& hsl, RGBAColorF& rgba );
CMYKColor ConvertRGBtoCMYK( float64 r, float64 g, float64 b );
RGBColor ConvertCMYKtoRGB( float64 c, float64 m, float64 y, float64 k );
HSLColor ConvertRGBtoHSL( float64 r, float64 g, float64 b );
RGBColor ConvertHSLtoRGB( float64 h, float64 s, float64 l );
CMYKColor ConvertHSLtoCMYK( float64 h, float64 s, float64 l );
HSLColor ConvertCMYKtoHSL( float64 c, float64 m, float64 y, float64 k );
CMYKColorF ConvertRGBtoCMYK( float32 r, float32 g, float32 b );
RGBColorF ConvertCMYKtoRGB( float32 c, float32 m, float32 y, float32 k );
HSLColorF ConvertRGBtoHSL( float32 r, float32 g, float32 b );
RGBColorF ConvertHSLtoRGB( float32 h, float32 s, float32 l );
CMYKColorF ConvertHSLtoCMYK( float32 h, float32 s, float32 l );
HSLColorF ConvertCMYKtoHSL( float32 c, float32 m, float32 y, float32 k );
CMYKColor ConvertRGBtoCMYK( const RGBColor& rgb );
RGBColor ConvertCMYKtoRGB( const CMYKColor& cmyk );
HSLColor ConvertRGBtoHSL( const RGBColor& rgb );
RGBColor ConvertHSLtoRGB( const HSLColor& hsl );
CMYKColor ConvertHSLtoCMYK( const HSLColor& hsl );
HSLColor ConvertCMYKtoHSL( const CMYKColor& cmyk );
CMYKColorF ConvertRGBtoCMYK( const RGBColorF& rgb );
RGBColorF ConvertCMYKtoRGB( const CMYKColorF& cmyk );
HSLColorF ConvertRGBtoHSL( const RGBColorF& rgb );
RGBColorF ConvertHSLtoRGB( const HSLColorF& hsl );
CMYKColorF ConvertHSLtoCMYK( const HSLColorF& hsl );
HSLColorF ConvertCMYKtoHSL( const CMYKColorF& cmyk );
CMYKColor ConvertRGBAtoCMYK( const RGBAColor& rgba );
RGBAColor ConvertCMYKtoRGBA( const CMYKColor& cmyk );
HSLColor ConvertRGBAtoHSL( const RGBAColor& rgba );
RGBAColor ConvertHSLtoRGBA( const HSLColor& hsl );
CMYKColorF ConvertRGBAtoCMYK( const RGBAColorF& rgba );
RGBAColorF ConvertCMYKtoRGBA( const CMYKColorF& cmyk );
HSLColorF ConvertRGBAtoHSL( const RGBAColorF& rgba );
RGBAColorF ConvertHSLtoRGBA( const HSLColorF& hsl );

std::ostream& operator<<( std::ostream&, const Color& );
std::ostream& operator<<( std::ostream&, Color_ostream_state );