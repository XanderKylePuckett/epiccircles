#pragma once

typedef short SHORT;
typedef int BOOL;

enum ConsoleColor
{
	Black, DarkBlue, DarkGreen, DarkCyan,
	DarkRed, DarkMagenta, DarkYellow, Gray,
	DarkGray, Blue, Green, Cyan,
	Red, Magenta, Yellow, White
};

class Console
{
public:
	static ConsoleColor GetForeground( void );
	static void SetForeground( ConsoleColor );
	static ConsoleColor GetBackground( void );
	static void SetBackground( ConsoleColor );
	static void SetColors( ConsoleColor _fg, ConsoleColor _bg );
	static void GetColors( ConsoleColor& _fg, ConsoleColor& _bg );
	static void ResetColor( void );
	static uint32 WindowWidth( void );
	static uint32 WindowHeight( void );
	static void SetWindowSize( uint32 _width, uint32 _height );
	static void SetBufferSize( uint32 _width, uint32 _height );
	static SHORT CursorLeft( void );
	static SHORT CursorTop( void );
	static void SetCursorPosition( SHORT _col, SHORT _row );
	static void Clear( void );
	static void CursorVisible( BOOL );
	static void Lock( bool );
	static void EOLWrap( bool );
	static void FlushKeys( void );
	static void Show( SHORT _x, SHORT _y, wchar_t _symbol );
	static void DrawBox( SHORT _left, SHORT _top, SHORT _width, SHORT _height, bool _dbl );
	static const char* RandomName( void );
	static void WordWrap( SHORT _x, SHORT _y, uint32 _width, const char* const _text );
};