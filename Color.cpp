#include "stdafx.h"
#include "Color.h"
#include "Math.h"
#include <iomanip>

const Color Color::ZERO = Color( 0x00000000u );
const Color Color::BLACK = Color( 0xff000000u );
const Color Color::BLUE = Color( 0xff0000ffu );
const Color Color::GREEN = Color( 0xff00ff00u );
const Color Color::CYAN = Color( 0xff00ffffu );
const Color Color::RED = Color( 0xffff0000u );
const Color Color::MAGENTA = Color( 0xffff00ffu );
const Color Color::YELLOW = Color( 0xffffff00u );
const Color Color::WHITE = Color( 0xffffffffu );
const Color Color::ORANGE = Color( 0xffffa500u );
const Color Color::PURPLE = Color( 0xff800080u );
const Color Color::PINK = Color( 0xffffc0cbu );
const Color Color::HOT_PINK = Color( 0xffff69b4u );
const Color Color::VIOLET = Color( 0xffee82eeu );
const Color Color::INDIGO = Color( 0xff4b0082u );
const Color Color::LAVENDER = Color( 0xffe6e6fau );
const Color Color::SKY_BLUE = Color( 0xff87ceebu );
const Color Color::NAVY_BLUE = Color( 0xff000080u );
const Color Color::SILVER = Color( 0xffc0c0c0u );
const Color Color::SEA_BLUE = Color( 0xff006994u );
const Color Color::SEA_GREEN = Color( 0xff2e8b57u );
const Color Color::SAPPHIRE = Color( 0xff0f52bau );
const Color Color::SCARLET = Color( 0xffff2400u );
const Color Color::BROWN = Color( 0xffa52a2au );
const Color Color::DARK_BROWN = Color( 0xff654321u );
const Color Color::PEACH = Color( 0xffffe5b4u );
const Color Color::PERIDOT = Color( 0xffe6e200u );
const Color Color::ROSE = Color( 0xffff007fu );
const Color Color::ROSE_PINK = Color( 0xffff66ccu );
const Color Color::ROSE_QUARTZ = Color( 0xffaa98a9u );
const Color Color::PEARL = Color( 0xffeae0c8u );
const Color Color::PINK_PEARL = Color( 0xffe7accfu );
const Color Color::PEARL_AQUA = Color( 0xff88d8c0u );
const Color Color::TAN = Color( 0xffd2b48cu );
const Color Color::AMBER = Color( 0xffffbf00u );
const Color Color::GRAY = Color( 0xff808080u );
const Color Color::GOLD = Color( 0xffffd700u );
const Color Color::DEEP_PINK = Color( 0xffff1493u );
const Color Color::RUBY = Color( 0xffe0115fu );
const Color Color::CHERRY = Color( 0xffde3163u );
const Color Color::CLASSIC_ROSE = Color( 0xfffbcce7u );
const Color Color::COPPER = Color( 0xffb87333u );
const Color Color::CRIMSON = Color( 0xffdc143cu );
const Color Color::CRIMSON_RED = Color( 0xff990000u );
const Color Color::CERULEAN = Color( 0xff007ba7u );
const Color Color::OLIVE = Color( 0xff808000u );
const Color Color::MAROON = Color( 0xff800000u );
const Color Color::AQUAMARINE = Color( 0xff7fffd4u );
const Color Color::MEDIUM_AQUAMARINE = Color( 0xff66ddaau );
const Color Color::MAUVE = Color( 0xffe0b0ffu );
const Color Color::TEAL = Color( 0xff008080u );
const Color Color::TURQUOISE = Color( 0xff30d5c8u );
const Color Color::LIME = Color( 0xffbfff00u );
const Color Color::LIME_GREEN = Color( 0xff32cd32u );
const Color Color::LILAC = Color( 0xffc8a2c8u );
const Color Color::ELECTRIC_LIME = Color( 0xffccff00u );
const Color Color::AZURE = Color( 0xff007fffu );
const Color Color::CHARTREUSE = Color( 0xff7fff00u );
const Color Color::LIGHT_GRAY = Color( 0xffd3d3d3u );
const Color Color::APRICOT = Color( 0xfffbceb1u );
const Color Color::OCHRE = Color( 0xffcc7722u );
const Color Color::PLUM = Color( 0xffdda0ddu );
const Color Color::DARK_GRAY = Color( 0xffa9a9a9u );
const Color Color::BLUE_GREEN = Color( 0xff0d98bau );
const Color Color::BLUE_VIOLET = Color( 0xff8a2be2u );
const Color Color::RED_ORANGE = Color( 0xffff5349u );
const Color Color::ORANGE_RED = Color( 0xffff4500u );
const Color Color::RED_VIOLET = Color( 0xffc71585u );
const Color Color::AMETHYST = Color( 0xff9966ccu );
const Color Color::DARK_RED = Color( 0xff8b0000u );
const Color Color::DARK_GREEN = Color( 0xff013220u );
const Color Color::DARK_BLUE = Color( 0xff00008bu );
const Color Color::DARK_CYAN = Color( 0xff008b8bu );
const Color Color::DARK_MAGENTA = Color( 0xff8b008bu );
const Color Color::LIGHT_GREEN = Color( 0xff90ee90u );
const Color Color::LIGHT_BLUE = Color( 0xffadd8e6u );
const Color Color::LIGHT_CYAN = Color( 0xffe0ffffu );
const Color Color::LIGHT_YELLOW = Color( 0xffffffedu );
const Color Color::TANGELO = Color( 0xfff94d00u );
const Color Color::BRONZE = Color( 0xffcd7f32u );
const Color Color::XANADU = Color( 0xff738678u );

static const uint64 inv255U[ 256 ] =
{
	0x0000000000000000ull, 0x3f70101010101010ull, 0x3f80101010101010ull, 0x3f88181818181818ull,
	0x3f90101010101010ull, 0x3f94141414141414ull, 0x3f98181818181818ull, 0x3f9c1c1c1c1c1c1cull,
	0x3fa0101010101010ull, 0x3fa2121212121212ull, 0x3fa4141414141414ull, 0x3fa6161616161616ull,
	0x3fa8181818181818ull, 0x3faa1a1a1a1a1a1aull, 0x3fac1c1c1c1c1c1cull, 0x3fae1e1e1e1e1e1eull,
	0x3fb0101010101010ull, 0x3fb1111111111111ull, 0x3fb2121212121212ull, 0x3fb3131313131313ull,
	0x3fb4141414141414ull, 0x3fb5151515151515ull, 0x3fb6161616161616ull, 0x3fb7171717171717ull,
	0x3fb8181818181818ull, 0x3fb9191919191919ull, 0x3fba1a1a1a1a1a1aull, 0x3fbb1b1b1b1b1b1bull,
	0x3fbc1c1c1c1c1c1cull, 0x3fbd1d1d1d1d1d1dull, 0x3fbe1e1e1e1e1e1eull, 0x3fbf1f1f1f1f1f1full,
	0x3fc0101010101010ull, 0x3fc0909090909091ull, 0x3fc1111111111111ull, 0x3fc1919191919192ull,
	0x3fc2121212121212ull, 0x3fc2929292929293ull, 0x3fc3131313131313ull, 0x3fc3939393939394ull,
	0x3fc4141414141414ull, 0x3fc4949494949495ull, 0x3fc5151515151515ull, 0x3fc5959595959596ull,
	0x3fc6161616161616ull, 0x3fc6969696969697ull, 0x3fc7171717171717ull, 0x3fc7979797979798ull,
	0x3fc8181818181818ull, 0x3fc8989898989899ull, 0x3fc9191919191919ull, 0x3fc999999999999aull,
	0x3fca1a1a1a1a1a1aull, 0x3fca9a9a9a9a9a9bull, 0x3fcb1b1b1b1b1b1bull, 0x3fcb9b9b9b9b9b9cull,
	0x3fcc1c1c1c1c1c1cull, 0x3fcc9c9c9c9c9c9dull, 0x3fcd1d1d1d1d1d1dull, 0x3fcd9d9d9d9d9d9eull,
	0x3fce1e1e1e1e1e1eull, 0x3fce9e9e9e9e9e9full, 0x3fcf1f1f1f1f1f1full, 0x3fcf9f9f9f9f9fa0ull,
	0x3fd0101010101010ull, 0x3fd0505050505050ull, 0x3fd0909090909091ull, 0x3fd0d0d0d0d0d0d1ull,
	0x3fd1111111111111ull, 0x3fd1515151515151ull, 0x3fd1919191919192ull, 0x3fd1d1d1d1d1d1d2ull,
	0x3fd2121212121212ull, 0x3fd2525252525252ull, 0x3fd2929292929293ull, 0x3fd2d2d2d2d2d2d3ull,
	0x3fd3131313131313ull, 0x3fd3535353535353ull, 0x3fd3939393939394ull, 0x3fd3d3d3d3d3d3d4ull,
	0x3fd4141414141414ull, 0x3fd4545454545454ull, 0x3fd4949494949495ull, 0x3fd4d4d4d4d4d4d5ull,
	0x3fd5151515151515ull, 0x3fd5555555555555ull, 0x3fd5959595959596ull, 0x3fd5d5d5d5d5d5d6ull,
	0x3fd6161616161616ull, 0x3fd6565656565656ull, 0x3fd6969696969697ull, 0x3fd6d6d6d6d6d6d7ull,
	0x3fd7171717171717ull, 0x3fd7575757575757ull, 0x3fd7979797979798ull, 0x3fd7d7d7d7d7d7d8ull,
	0x3fd8181818181818ull, 0x3fd8585858585858ull, 0x3fd8989898989899ull, 0x3fd8d8d8d8d8d8d9ull,
	0x3fd9191919191919ull, 0x3fd9595959595959ull, 0x3fd999999999999aull, 0x3fd9d9d9d9d9d9daull,
	0x3fda1a1a1a1a1a1aull, 0x3fda5a5a5a5a5a5aull, 0x3fda9a9a9a9a9a9bull, 0x3fdadadadadadadbull,
	0x3fdb1b1b1b1b1b1bull, 0x3fdb5b5b5b5b5b5bull, 0x3fdb9b9b9b9b9b9cull, 0x3fdbdbdbdbdbdbdcull,
	0x3fdc1c1c1c1c1c1cull, 0x3fdc5c5c5c5c5c5cull, 0x3fdc9c9c9c9c9c9dull, 0x3fdcdcdcdcdcdcddull,
	0x3fdd1d1d1d1d1d1dull, 0x3fdd5d5d5d5d5d5dull, 0x3fdd9d9d9d9d9d9eull, 0x3fdddddddddddddeull,
	0x3fde1e1e1e1e1e1eull, 0x3fde5e5e5e5e5e5eull, 0x3fde9e9e9e9e9e9full, 0x3fdededededededfull,
	0x3fdf1f1f1f1f1f1full, 0x3fdf5f5f5f5f5f5full, 0x3fdf9f9f9f9f9fa0ull, 0x3fdfdfdfdfdfdfe0ull,
	0x3fe0101010101010ull, 0x3fe0303030303030ull, 0x3fe0505050505050ull, 0x3fe0707070707070ull,
	0x3fe0909090909091ull, 0x3fe0b0b0b0b0b0b1ull, 0x3fe0d0d0d0d0d0d1ull, 0x3fe0f0f0f0f0f0f1ull,
	0x3fe1111111111111ull, 0x3fe1313131313131ull, 0x3fe1515151515151ull, 0x3fe1717171717171ull,
	0x3fe1919191919192ull, 0x3fe1b1b1b1b1b1b2ull, 0x3fe1d1d1d1d1d1d2ull, 0x3fe1f1f1f1f1f1f2ull,
	0x3fe2121212121212ull, 0x3fe2323232323232ull, 0x3fe2525252525252ull, 0x3fe2727272727272ull,
	0x3fe2929292929293ull, 0x3fe2b2b2b2b2b2b3ull, 0x3fe2d2d2d2d2d2d3ull, 0x3fe2f2f2f2f2f2f3ull,
	0x3fe3131313131313ull, 0x3fe3333333333333ull, 0x3fe3535353535353ull, 0x3fe3737373737373ull,
	0x3fe3939393939394ull, 0x3fe3b3b3b3b3b3b4ull, 0x3fe3d3d3d3d3d3d4ull, 0x3fe3f3f3f3f3f3f4ull,
	0x3fe4141414141414ull, 0x3fe4343434343434ull, 0x3fe4545454545454ull, 0x3fe4747474747474ull,
	0x3fe4949494949495ull, 0x3fe4b4b4b4b4b4b5ull, 0x3fe4d4d4d4d4d4d5ull, 0x3fe4f4f4f4f4f4f5ull,
	0x3fe5151515151515ull, 0x3fe5353535353535ull, 0x3fe5555555555555ull, 0x3fe5757575757575ull,
	0x3fe5959595959596ull, 0x3fe5b5b5b5b5b5b6ull, 0x3fe5d5d5d5d5d5d6ull, 0x3fe5f5f5f5f5f5f6ull,
	0x3fe6161616161616ull, 0x3fe6363636363636ull, 0x3fe6565656565656ull, 0x3fe6767676767676ull,
	0x3fe6969696969697ull, 0x3fe6b6b6b6b6b6b7ull, 0x3fe6d6d6d6d6d6d7ull, 0x3fe6f6f6f6f6f6f7ull,
	0x3fe7171717171717ull, 0x3fe7373737373737ull, 0x3fe7575757575757ull, 0x3fe7777777777777ull,
	0x3fe7979797979798ull, 0x3fe7b7b7b7b7b7b8ull, 0x3fe7d7d7d7d7d7d8ull, 0x3fe7f7f7f7f7f7f8ull,
	0x3fe8181818181818ull, 0x3fe8383838383838ull, 0x3fe8585858585858ull, 0x3fe8787878787878ull,
	0x3fe8989898989899ull, 0x3fe8b8b8b8b8b8b9ull, 0x3fe8d8d8d8d8d8d9ull, 0x3fe8f8f8f8f8f8f9ull,
	0x3fe9191919191919ull, 0x3fe9393939393939ull, 0x3fe9595959595959ull, 0x3fe9797979797979ull,
	0x3fe999999999999aull, 0x3fe9b9b9b9b9b9baull, 0x3fe9d9d9d9d9d9daull, 0x3fe9f9f9f9f9f9faull,
	0x3fea1a1a1a1a1a1aull, 0x3fea3a3a3a3a3a3aull, 0x3fea5a5a5a5a5a5aull, 0x3fea7a7a7a7a7a7aull,
	0x3fea9a9a9a9a9a9bull, 0x3feababababababbull, 0x3feadadadadadadbull, 0x3feafafafafafafbull,
	0x3feb1b1b1b1b1b1bull, 0x3feb3b3b3b3b3b3bull, 0x3feb5b5b5b5b5b5bull, 0x3feb7b7b7b7b7b7bull,
	0x3feb9b9b9b9b9b9cull, 0x3febbbbbbbbbbbbcull, 0x3febdbdbdbdbdbdcull, 0x3febfbfbfbfbfbfcull,
	0x3fec1c1c1c1c1c1cull, 0x3fec3c3c3c3c3c3cull, 0x3fec5c5c5c5c5c5cull, 0x3fec7c7c7c7c7c7cull,
	0x3fec9c9c9c9c9c9dull, 0x3fecbcbcbcbcbcbdull, 0x3fecdcdcdcdcdcddull, 0x3fecfcfcfcfcfcfdull,
	0x3fed1d1d1d1d1d1dull, 0x3fed3d3d3d3d3d3dull, 0x3fed5d5d5d5d5d5dull, 0x3fed7d7d7d7d7d7dull,
	0x3fed9d9d9d9d9d9eull, 0x3fedbdbdbdbdbdbeull, 0x3feddddddddddddeull, 0x3fedfdfdfdfdfdfeull,
	0x3fee1e1e1e1e1e1eull, 0x3fee3e3e3e3e3e3eull, 0x3fee5e5e5e5e5e5eull, 0x3fee7e7e7e7e7e7eull,
	0x3fee9e9e9e9e9e9full, 0x3feebebebebebebfull, 0x3feededededededfull, 0x3feefefefefefeffull,
	0x3fef1f1f1f1f1f1full, 0x3fef3f3f3f3f3f3full, 0x3fef5f5f5f5f5f5full, 0x3fef7f7f7f7f7f7full,
	0x3fef9f9f9f9f9fa0ull, 0x3fefbfbfbfbfbfc0ull, 0x3fefdfdfdfdfdfe0ull, 0x3ff0000000000000ull
};
static const float64* const inv255 = ( float64* )inv255U;

static const uint32 inv255fU[ 256 ] =
{
	0x00000000u, 0x3b808081u, 0x3c008081u, 0x3c40c0c1u, 0x3c808081u, 0x3ca0a0a1u, 0x3cc0c0c1u, 0x3ce0e0e1u,
	0x3d008081u, 0x3d109091u, 0x3d20a0a1u, 0x3d30b0b1u, 0x3d40c0c1u, 0x3d50d0d1u, 0x3d60e0e1u, 0x3d70f0f1u,
	0x3d808081u, 0x3d888889u, 0x3d909091u, 0x3d989899u, 0x3da0a0a1u, 0x3da8a8a9u, 0x3db0b0b1u, 0x3db8b8b9u,
	0x3dc0c0c1u, 0x3dc8c8c9u, 0x3dd0d0d1u, 0x3dd8d8d9u, 0x3de0e0e1u, 0x3de8e8e9u, 0x3df0f0f1u, 0x3df8f8f9u,
	0x3e008081u, 0x3e048485u, 0x3e088889u, 0x3e0c8c8du, 0x3e109091u, 0x3e149495u, 0x3e189899u, 0x3e1c9c9du,
	0x3e20a0a1u, 0x3e24a4a5u, 0x3e28a8a9u, 0x3e2cacadu, 0x3e30b0b1u, 0x3e34b4b5u, 0x3e38b8b9u, 0x3e3cbcbdu,
	0x3e40c0c1u, 0x3e44c4c5u, 0x3e48c8c9u, 0x3e4ccccdu, 0x3e50d0d1u, 0x3e54d4d5u, 0x3e58d8d9u, 0x3e5cdcddu,
	0x3e60e0e1u, 0x3e64e4e5u, 0x3e68e8e9u, 0x3e6cecedu, 0x3e70f0f1u, 0x3e74f4f5u, 0x3e78f8f9u, 0x3e7cfcfdu,
	0x3e808081u, 0x3e828283u, 0x3e848485u, 0x3e868687u, 0x3e888889u, 0x3e8a8a8bu, 0x3e8c8c8du, 0x3e8e8e8fu,
	0x3e909091u, 0x3e929293u, 0x3e949495u, 0x3e969697u, 0x3e989899u, 0x3e9a9a9bu, 0x3e9c9c9du, 0x3e9e9e9fu,
	0x3ea0a0a1u, 0x3ea2a2a3u, 0x3ea4a4a5u, 0x3ea6a6a7u, 0x3ea8a8a9u, 0x3eaaaaabu, 0x3eacacadu, 0x3eaeaeafu,
	0x3eb0b0b1u, 0x3eb2b2b3u, 0x3eb4b4b5u, 0x3eb6b6b7u, 0x3eb8b8b9u, 0x3ebababbu, 0x3ebcbcbdu, 0x3ebebebfu,
	0x3ec0c0c1u, 0x3ec2c2c3u, 0x3ec4c4c5u, 0x3ec6c6c7u, 0x3ec8c8c9u, 0x3ecacacbu, 0x3ecccccdu, 0x3ecececfu,
	0x3ed0d0d1u, 0x3ed2d2d3u, 0x3ed4d4d5u, 0x3ed6d6d7u, 0x3ed8d8d9u, 0x3edadadbu, 0x3edcdcddu, 0x3edededfu,
	0x3ee0e0e1u, 0x3ee2e2e3u, 0x3ee4e4e5u, 0x3ee6e6e7u, 0x3ee8e8e9u, 0x3eeaeaebu, 0x3eececedu, 0x3eeeeeefu,
	0x3ef0f0f1u, 0x3ef2f2f3u, 0x3ef4f4f5u, 0x3ef6f6f7u, 0x3ef8f8f9u, 0x3efafafbu, 0x3efcfcfdu, 0x3efefeffu,
	0x3f008081u, 0x3f018182u, 0x3f028283u, 0x3f038384u, 0x3f048485u, 0x3f058586u, 0x3f068687u, 0x3f078788u,
	0x3f088889u, 0x3f09898au, 0x3f0a8a8bu, 0x3f0b8b8cu, 0x3f0c8c8du, 0x3f0d8d8eu, 0x3f0e8e8fu, 0x3f0f8f90u,
	0x3f109091u, 0x3f119192u, 0x3f129293u, 0x3f139394u, 0x3f149495u, 0x3f159596u, 0x3f169697u, 0x3f179798u,
	0x3f189899u, 0x3f19999au, 0x3f1a9a9bu, 0x3f1b9b9cu, 0x3f1c9c9du, 0x3f1d9d9eu, 0x3f1e9e9fu, 0x3f1f9fa0u,
	0x3f20a0a1u, 0x3f21a1a2u, 0x3f22a2a3u, 0x3f23a3a4u, 0x3f24a4a5u, 0x3f25a5a6u, 0x3f26a6a7u, 0x3f27a7a8u,
	0x3f28a8a9u, 0x3f29a9aau, 0x3f2aaaabu, 0x3f2babacu, 0x3f2cacadu, 0x3f2dadaeu, 0x3f2eaeafu, 0x3f2fafb0u,
	0x3f30b0b1u, 0x3f31b1b2u, 0x3f32b2b3u, 0x3f33b3b4u, 0x3f34b4b5u, 0x3f35b5b6u, 0x3f36b6b7u, 0x3f37b7b8u,
	0x3f38b8b9u, 0x3f39b9bau, 0x3f3ababbu, 0x3f3bbbbcu, 0x3f3cbcbdu, 0x3f3dbdbeu, 0x3f3ebebfu, 0x3f3fbfc0u,
	0x3f40c0c1u, 0x3f41c1c2u, 0x3f42c2c3u, 0x3f43c3c4u, 0x3f44c4c5u, 0x3f45c5c6u, 0x3f46c6c7u, 0x3f47c7c8u,
	0x3f48c8c9u, 0x3f49c9cau, 0x3f4acacbu, 0x3f4bcbccu, 0x3f4ccccdu, 0x3f4dcdceu, 0x3f4ececfu, 0x3f4fcfd0u,
	0x3f50d0d1u, 0x3f51d1d2u, 0x3f52d2d3u, 0x3f53d3d4u, 0x3f54d4d5u, 0x3f55d5d6u, 0x3f56d6d7u, 0x3f57d7d8u,
	0x3f58d8d9u, 0x3f59d9dau, 0x3f5adadbu, 0x3f5bdbdcu, 0x3f5cdcddu, 0x3f5ddddeu, 0x3f5ededfu, 0x3f5fdfe0u,
	0x3f60e0e1u, 0x3f61e1e2u, 0x3f62e2e3u, 0x3f63e3e4u, 0x3f64e4e5u, 0x3f65e5e6u, 0x3f66e6e7u, 0x3f67e7e8u,
	0x3f68e8e9u, 0x3f69e9eau, 0x3f6aeaebu, 0x3f6bebecu, 0x3f6cecedu, 0x3f6dedeeu, 0x3f6eeeefu, 0x3f6feff0u,
	0x3f70f0f1u, 0x3f71f1f2u, 0x3f72f2f3u, 0x3f73f3f4u, 0x3f74f4f5u, 0x3f75f5f6u, 0x3f76f6f7u, 0x3f77f7f8u,
	0x3f78f8f9u, 0x3f79f9fau, 0x3f7afafbu, 0x3f7bfbfcu, 0x3f7cfcfdu, 0x3f7dfdfeu, 0x3f7efeffu, 0x3f800000u
};
static const float32* const inv255f = ( float32* )inv255fU;

float64 GetChannel( uint32 val )
{
	return inv255[ val & 0x000000ffu ];
}
float32 GetChannelF( uint32 val )
{
	return inv255f[ val & 0x000000ffu ];
}
uint32 GetUintChannel( float64 _val )
{
	return Math::Min( ( uint32 )( Math::Clamp01( _val ) * 256.0 ), 0x000000ffu );
}
uint32 GetUintChannel( float32 _val )
{
	return GetUintChannel( ( float64 )_val );
}
uint32 GetUintColor( float64 r, float64 g, float64 b, float64 a )
{
	return ( ( GetUintChannel( a ) << 24 )
			 | ( GetUintChannel( r ) << 16 )
			 | ( GetUintChannel( g ) << 8 )
			 | GetUintChannel( b ) );
}
uint32 GetUintColor( float64 r, float64 g, float64 b )
{
	return ( 0xff000000u
			 | ( GetUintChannel( r ) << 16 )
			 | ( GetUintChannel( g ) << 8 )
			 | GetUintChannel( b ) );
}
void GetChannels( uint32 argb, float64& r, float64& g, float64& b, float64& a )
{
	r = GetChannel( argb >> 16 );
	g = GetChannel( argb >> 8 );
	b = GetChannel( argb );
	a = GetChannel( argb >> 24 );
}
void GetChannels( uint32 argb, float64& r, float64& g, float64& b )
{
	r = GetChannel( argb >> 16 );
	g = GetChannel( argb >> 8 );
	b = GetChannel( argb );
}
uint32 GetUintColor( float32 _r, float32 _g, float32 _b, float32 _a )
{
	return GetUintColor( ( float64 )_r, ( float64 )_g, ( float64 )_b, ( float64 )_a );
}
uint32 GetUintColor( float32 _r, float32 _g, float32 _b )
{
	return GetUintColor( ( float64 )_r, ( float64 )_g, ( float64 )_b );
}
void GetChannels( uint32 argb, float32& r, float32& g, float32& b, float32& a )
{
	r = GetChannelF( argb >> 16 );
	g = GetChannelF( argb >> 8 );
	b = GetChannelF( argb );
	a = GetChannelF( argb >> 24 );
}
void GetChannels( uint32 argb, float32& r, float32& g, float32& b )
{
	r = GetChannelF( argb >> 16 );
	g = GetChannelF( argb >> 8 );
	b = GetChannelF( argb );
}
void GetChannels( uint32 argb, uint32& r, uint32& g, uint32& b, uint32& a )
{
	r = ( argb >> 16 ) & 0x000000ffu;
	g = ( argb >> 8 ) & 0x000000ffu;
	b = ( argb ) & 0x000000ffu;
	a = ( argb >> 24 ) & 0x000000ffu;
}
void GetChannels( uint32 argb, uint32& r, uint32& g, uint32& b )
{
	r = ( argb >> 16 ) & 0x000000ffu;
	g = ( argb >> 8 ) & 0x000000ffu;
	b = ( argb ) & 0x000000ffu;
}
uint32 GetUintColor( uint32 r, uint32 g, uint32 b, uint32 a = 0x000000ffu )
{
	return ( ( ( a & 0x000000ffu ) << 24 )
			 | ( ( r & 0x000000ffu ) << 16 )
			 | ( ( g & 0x000000ffu ) << 8 )
			 | ( b & 0x000000ffu ) );
}
uint32 WebSafeChannel( uint32 _channel )
{
	_channel &= 255u;
	if ( _channel < 26u )
		return 0u;
	if ( _channel < 77u )
		return 51u;
	if ( _channel < 128u )
		return 102u;
	if ( _channel < 179u )
		return 153u;
	if ( _channel < 230u )
		return 204u;
	return 255u;
}
Color Color::GetWebSafeColor( void ) const
{
	uint32 r_, g_, b_, a_;
	GetChannels( m_argb, r_, g_, b_, a_ );
	r_ = WebSafeChannel( r_ );
	g_ = WebSafeChannel( g_ );
	b_ = WebSafeChannel( b_ );
	return Color( GetUintColor( r_, g_, b_, a_ ) );
}
Color Color::SimulateColorblind( ColorblindFilter _filter ) const
{
	if ( ColorblindFilter::COLORBLIND_NORMAL == _filter )
		return Color( m_argb );
	const RGBAColor rgba_ = RGBAColor( *this );
	switch ( _filter )
	{
		case ColorblindFilter::COLORBLIND_PROTANOPIA:
			return Color(
				rgba_.red * 0.566666667 + rgba_.green * 0.433333333,
				rgba_.red * 0.558333333 + rgba_.green * 0.441666667,
				rgba_.green * 0.241666667 + rgba_.blue * 0.758333333,
				rgba_.alpha );
		case ColorblindFilter::COLORBLIND_PROTANOMALY:
			return Color(
				rgba_.red * 0.816666667 + rgba_.green * 0.183333333,
				rgba_.red * 0.333333333 + rgba_.green * 0.666666667,
				rgba_.green * 0.125 + rgba_.blue * 0.875,
				rgba_.alpha );
		case ColorblindFilter::COLORBLIND_DEUTERANOPIA:
			return Color(
				rgba_.red * 0.625 + rgba_.green * 0.375,
				rgba_.red * 0.7 + rgba_.green * 0.3,
				rgba_.green * 0.3 + rgba_.blue * 0.7,
				rgba_.alpha );
		case ColorblindFilter::COLORBLIND_DEUTERANOMALY:
			return Color(
				rgba_.red * 0.8 + rgba_.green * 0.2,
				rgba_.red * 0.258333333 + rgba_.green * 0.741666667,
				rgba_.green * 0.141666667 + rgba_.blue * 0.858333333,
				rgba_.alpha );
		case ColorblindFilter::COLORBLIND_TRITANOPIA:
			return Color(
				rgba_.red * 0.95 + rgba_.green * 0.05,
				rgba_.green * 0.433333333 + rgba_.blue * 0.566666667,
				rgba_.green * 0.475 + rgba_.blue * 0.525,
				rgba_.alpha );
		case ColorblindFilter::COLORBLIND_TRITANOMALY:
			return Color(
				rgba_.red * 0.966666667 + rgba_.green * 0.033333333,
				rgba_.green * 0.733333333 + rgba_.blue * 0.266666667,
				rgba_.green * 0.183333333 + rgba_.blue * 0.816666667,
				rgba_.alpha );
		case ColorblindFilter::COLORBLIND_ACHROMATOPSIA:
		{
			const float64 chan = rgba_.red * 0.299 + rgba_.green * 0.587 + rgba_.blue * 0.114;
			return Color( chan, chan, chan, rgba_.alpha );
		}
		case ColorblindFilter::COLORBLIND_ACHROMATOMALY:
			return Color(
				rgba_.red * 0.618 + rgba_.green * 0.32 + rgba_.blue * 0.062,
				rgba_.red * 0.163 + rgba_.green * 0.775 + rgba_.blue * 0.062,
				rgba_.red * 0.163333333 + rgba_.green * 0.32 + rgba_.blue * 0.516666667,
				rgba_.alpha );
		default:
			break;
	}
	return Color( m_argb );
}
Color::Color( void )
{
}
Color::Color( uint32 argb ) :
	m_argb( argb )
{
}
Color::Color( float64 r, float64 g, float64 b, float64 a )
{
	m_argb = GetUintColor( r, g, b, a );
}
Color::Color( float32 r, float32 g, float32 b, float32 a )
{
	m_argb = GetUintColor( r, g, b, a );
}
Color::Color( const RGBColor& rhs )
{
	m_argb = GetUintColor( rhs.red, rhs.green, rhs.blue );
}
Color::Color( const RGBAColor& rhs )
{
	m_argb = GetUintColor( rhs.red, rhs.green, rhs.blue, rhs.alpha );
}
Color::Color( const CMYKColor& rhs )
{
	const RGBColor rgb = ConvertCMYKtoRGB( rhs );
	m_argb = GetUintColor( rgb.red, rgb.green, rgb.blue );
}
Color::Color( const HSLColor& rhs )
{
	const RGBColor rgb = ConvertHSLtoRGB( rhs );
	m_argb = GetUintColor( rgb.red, rgb.green, rgb.blue );
}
Color::Color( const RGBColorF& rhs )
{
	m_argb = GetUintColor( rhs.red, rhs.green, rhs.blue );
}
Color::Color( const RGBAColorF& rhs )
{
	m_argb = GetUintColor( rhs.red, rhs.green, rhs.blue, rhs.alpha );
}
Color::Color( const CMYKColorF& rhs )
{
	const RGBColorF rgb = ConvertCMYKtoRGB( rhs );
	m_argb = GetUintColor( rgb.red, rgb.green, rgb.blue );
}
Color::Color( const HSLColorF& rhs )
{
	const RGBColorF rgb = ConvertHSLtoRGB( rhs );
	m_argb = GetUintColor( rgb.red, rgb.green, rgb.blue );
}
RGBColor::RGBColor( void )
{
}
RGBColor::RGBColor( const Color& color )
{
	GetChannels( color.m_argb, red, green, blue );
}
RGBColor::RGBColor( float64 r, float64 g, float64 b ) :
	red( r ), green( g ), blue( b )
{
}
RGBAColor::RGBAColor( void )
{
}
RGBAColor::RGBAColor( const Color& color )
{
	GetChannels( color.m_argb, red, green, blue, alpha );
}
RGBAColor::RGBAColor( float64 r, float64 g, float64 b, float64 a ) :
	red( r ), green( g ), blue( b ), alpha( a )
{
}
CMYKColor::CMYKColor( void )
{
}
CMYKColor::CMYKColor( const Color& color )
{
	float64 r, g, b;
	GetChannels( color.m_argb, r, g, b );
	ConvertRGBtoCMYK( r, g, b, cyan, magenta, yellow, black );
}
CMYKColor::CMYKColor( float64 c, float64 m, float64 y, float64 k ) :
	cyan( c ), magenta( m ), yellow( y ), black( k )
{
}
HSLColor::HSLColor( void )
{
}
HSLColor::HSLColor( const Color& color )
{
	float64 r, g, b;
	GetChannels( color.m_argb, r, g, b );
	ConvertRGBtoHSL( r, g, b, hue, saturation, lightness );
}
HSLColor::HSLColor( float64 h, float64 s, float64 l ) :
	hue( h ), saturation( s ), lightness( l )
{
}
RGBColorF::RGBColorF( void )
{
}
RGBColorF::RGBColorF( const Color& color )
{
	GetChannels( color.m_argb, red, green, blue );
}
RGBColorF::RGBColorF( float32 r, float32 g, float32 b ) :
	red( r ), green( g ), blue( b )
{
}
RGBAColorF::RGBAColorF( void )
{
}
RGBAColorF::RGBAColorF( const Color& color )
{
	GetChannels( color.m_argb, red, green, blue, alpha );
}
RGBAColorF::RGBAColorF( float32 r, float32 g, float32 b, float32 a ) :
	red( r ), green( g ), blue( b ), alpha( a )
{
}
CMYKColorF::CMYKColorF( void )
{
}
CMYKColorF::CMYKColorF( const Color& color )
{
	float32 r, g, b;
	GetChannels( color.m_argb, r, g, b );
	ConvertRGBtoCMYK( r, g, b, cyan, magenta, yellow, black );
}
CMYKColorF::CMYKColorF( float32 c, float32 m, float32 y, float32 k ) :
	cyan( c ), magenta( m ), yellow( y ), black( k )
{
}
HSLColorF::HSLColorF( void )
{
}
HSLColorF::HSLColorF( const Color& color )
{
	float32 r, g, b;
	GetChannels( color.m_argb, r, g, b );
	ConvertRGBtoHSL( r, g, b, hue, saturation, lightness );
}
HSLColorF::HSLColorF( float32 h, float32 s, float32 l ) :
	hue( h ), saturation( s ), lightness( l )
{
}
Color BlendColors( const Color& _dst, const Color& _src )
{
	return Color( BlendColors( RGBAColor( _dst ), RGBAColor( _src ) ) );
}
RGBAColorF BlendColors( const RGBAColorF& _dst, const RGBAColorF& _src )
{
	return RGBAColorF( BlendColors( RGBAColor( _dst ), RGBAColor( _src ) ) );
}
RGBAColor BlendColors( const RGBAColor& _dst, const RGBAColor& _src )
{
	if ( _src.alpha >= 1.0 )
		return _src;
	if ( _src.alpha <= 0.0 )
		return _dst;
	const float64 oneMinusSrcAlpha_ = 1.0 - _src.alpha;
	if ( _dst.alpha >= 1.0 )
		return RGBAColor(
		_src.red * _src.alpha + _dst.red * oneMinusSrcAlpha_,
		_src.green * _src.alpha + _dst.green * oneMinusSrcAlpha_,
		_src.blue * _src.alpha + _dst.blue * oneMinusSrcAlpha_ );
	RGBAColor outColor_;
	outColor_.alpha = _src.alpha + _dst.alpha * oneMinusSrcAlpha_;
	const float64 invAlpha_ = 1.0 / outColor_.alpha;
	outColor_.red = ( _src.red * _src.alpha + _dst.red * _dst.alpha * oneMinusSrcAlpha_ ) * invAlpha_;
	outColor_.green = ( _src.green * _src.alpha + _dst.green * _dst.alpha * oneMinusSrcAlpha_ ) * invAlpha_;
	outColor_.blue = ( _src.blue * _src.alpha + _dst.blue * _dst.alpha * oneMinusSrcAlpha_ ) * invAlpha_;
	return outColor_;
}
Color SimulateColorblind( const Color& _color, ColorblindFilter _filter )
{
	return _color.SimulateColorblind( _filter );
}
void ConvertRGBtoCMYK( float64 r, float64 g, float64 b, float64& c, float64& m, float64& y, float64& k )
{
	k = Math::Min( 1.0 - r, 1.0 - g, 1.0 - b );
	const float64 x = 1.0 / ( 1.0 - k );
	c = 1.0 - r * x;
	m = 1.0 - g * x;
	y = 1.0 - b * x;
}
void ConvertCMYKtoRGB( float64 c, float64 m, float64 y, float64 k, float64& r, float64& g, float64& b )
{
	k = 1.0 - k;
	r = k * ( 1.0 - c );
	g = k * ( 1.0 - m );
	b = k * ( 1.0 - y );
}
void ConvertRGBtoHSL( float64 r, float64 g, float64 b, float64& h, float64& s, float64& l )
{
	const float64 t = Math::Max( r, g, b );
	h = Math::Min( r, g, b );
	if ( t == h )
	{
		h = s = 0.0;
		l = t;
		return;
	}
	s = t + h;
	h = t - h;
	l = 0.5 * s;
	s = ( s <= 1.0 ) ? ( h / s ) : ( h / ( 2.0 - s ) );
	h = Const_1Div6 / h;
	if ( t == b )
		h = Math::Wrap01ex( Const_2Div3 + ( r - g ) * h );
	else if ( t == g )
		h = Math::Wrap01ex( Const_1Div3 + ( b - r ) * h );
	else
		h = Math::Wrap01ex( ( g - b ) * h );
}
void ConvertHSLtoRGB( float64 h, float64 s, float64 l, float64& r, float64& g, float64& b )
{
	if ( s <= 0.0 )
	{
		r = g = b = l;
		return;
	}
	const float64 t1 = ( l <= 0.5 ) ? ( l * s + l ) : ( ( 1.0 - l ) * s + l );
	const float64 t2 = 2.0 * l - t1;
	const float64 t3 = ( t1 - t2 ) * 6.0;
	r = Math::Wrap01ex( h + Const_1Div3 );
	g = Math::Wrap01ex( h );
	b = Math::Wrap01ex( h - Const_1Div3 );
	if ( r < Const_1Div6 ) r = t2 + r * t3;
	else if ( r <= 0.5 ) r = t1;
	else if ( r < Const_2Div3 ) r = t2 + ( Const_2Div3 - r ) * t3;
	else r = t2;
	if ( g < Const_1Div6 ) g = t2 + g * t3;
	else if ( g <= 0.5 ) g = t1;
	else if ( g < Const_2Div3 ) g = t2 + ( Const_2Div3 - g ) * t3;
	else g = t2;
	if ( b < Const_1Div6 ) b = t2 + b * t3;
	else if ( b <= 0.5 ) b = t1;
	else if ( b < Const_2Div3 ) b = t2 + ( Const_2Div3 - b ) * t3;
	else b = t2;
}
void ConvertHSLtoCMYK( float64 h, float64 s, float64 l, float64& c, float64& m, float64& y, float64& k )
{
	float64 r, g, b;
	ConvertHSLtoRGB( h, s, l, r, g, b );
	ConvertRGBtoCMYK( r, g, b, c, m, y, k );
}
void ConvertCMYKtoHSL( float64 c, float64 m, float64 y, float64 k, float64& h, float64& s, float64& l )
{
	float64 r, g, b;
	ConvertCMYKtoRGB( c, m, y, k, r, g, b );
	ConvertRGBtoHSL( r, g, b, h, s, l );
}
void ConvertRGBtoCMYK( float32 r, float32 g, float32 b, float32& c, float32& m, float32& y, float32& k )
{
	float64 c_, m_, y_, k_;
	ConvertRGBtoCMYK( ( float64 )r, ( float64 )g, ( float64 )b, c_, m_, y_, k_ );
	c = ( float32 )c_;
	m = ( float32 )m_;
	y = ( float32 )y_;
	k = ( float32 )k_;
}
void ConvertCMYKtoRGB( float32 c, float32 m, float32 y, float32 k, float32& r, float32& g, float32& b )
{
	float64 r_, g_, b_;
	ConvertCMYKtoRGB( ( float64 )c, ( float64 )m, ( float64 )y, ( float64 )k, r_, g_, b_ );
	r = ( float32 )r_;
	g = ( float32 )g_;
	b = ( float32 )b_;
}
void ConvertRGBtoHSL( float32 r, float32 g, float32 b, float32& h, float32& s, float32& l )
{
	float64 h_, s_, l_;
	ConvertRGBtoHSL( ( float64 )r, ( float64 )g, ( float64 )b, h_, s_, l_ );
	h = ( float32 )h_;
	s = ( float32 )s_;
	l = ( float32 )l_;
}
void ConvertHSLtoRGB( float32 h, float32 s, float32 l, float32& r, float32& g, float32& b )
{
	float64 r_, g_, b_;
	ConvertHSLtoRGB( ( float64 )h, ( float64 )s, ( float64 )l, r_, g_, b_ );
	r = ( float32 )r_;
	g = ( float32 )g_;
	b = ( float32 )b_;
}
void ConvertHSLtoCMYK( float32 h, float32 s, float32 l, float32& c, float32& m, float32& y, float32& k )
{
	float64 r, g, b;
	ConvertHSLtoRGB( ( float64 )h, ( float64 )s, ( float64 )l, r, g, b );
	ConvertRGBtoCMYK( r, g, b, c, m, y, k );
}
void ConvertCMYKtoHSL( float32 c, float32 m, float32 y, float32 k, float32& h, float32& s, float32& l )
{
	float64 r, g, b;
	ConvertCMYKtoRGB( ( float64 )c, ( float64 )m, ( float64 )y, ( float64 )k, r, g, b );
	ConvertRGBtoHSL( r, g, b, h, s, l );
}
void ConvertRGBtoCMYK( float32 r, float32 g, float32 b, float64& c, float64& m, float64& y, float64& k )
{
	ConvertRGBtoCMYK( ( float64 )r, ( float64 )g, ( float64 )b, c, m, y, k );
}
void ConvertCMYKtoRGB( float32 c, float32 m, float32 y, float32 k, float64& r, float64& g, float64& b )
{
	ConvertCMYKtoRGB( ( float64 )c, ( float64 )m, ( float64 )y, ( float64 )k, r, g, b );
}
void ConvertRGBtoHSL( float32 r, float32 g, float32 b, float64& h, float64& s, float64& l )
{
	ConvertRGBtoHSL( ( float64 )r, ( float64 )g, ( float64 )b, h, s, l );
}
void ConvertHSLtoRGB( float32 h, float32 s, float32 l, float64& r, float64& g, float64& b )
{
	ConvertHSLtoRGB( ( float64 )h, ( float64 )s, ( float64 )l, r, g, b );
}
void ConvertHSLtoCMYK( float32 h, float32 s, float32 l, float64& c, float64& m, float64& y, float64& k )
{
	float64 r, g, b;
	ConvertHSLtoRGB( ( float64 )h, ( float64 )s, ( float64 )l, r, g, b );
	ConvertRGBtoCMYK( r, g, b, c, m, y, k );
}
void ConvertCMYKtoHSL( float32 c, float32 m, float32 y, float32 k, float64& h, float64& s, float64& l )
{
	float64 r, g, b;
	ConvertCMYKtoRGB( ( float64 )c, ( float64 )m, ( float64 )y, ( float64 )k, r, g, b );
	ConvertRGBtoHSL( r, g, b, h, s, l );
}
void ConvertRGBtoCMYK( float64 r, float64 g, float64 b, float32& c, float32& m, float32& y, float32& k )
{
	float64 c_, m_, y_, k_;
	ConvertRGBtoCMYK( r, g, b, c_, m_, y_, k_ );
	c = ( float32 )c_;
	m = ( float32 )m_;
	y = ( float32 )y_;
	k = ( float32 )k_;
}
void ConvertCMYKtoRGB( float64 c, float64 m, float64 y, float64 k, float32& r, float32& g, float32& b )
{
	float64 r_, g_, b_;
	ConvertCMYKtoRGB( c, m, y, k, r_, g_, b_ );
	r = ( float32 )r_;
	g = ( float32 )g_;
	b = ( float32 )b_;
}
void ConvertRGBtoHSL( float64 r, float64 g, float64 b, float32& h, float32& s, float32& l )
{
	float64 h_, s_, l_;
	ConvertRGBtoHSL( r, g, b, h_, s_, l_ );
	h = ( float32 )h_;
	s = ( float32 )s_;
	l = ( float32 )l_;
}
void ConvertHSLtoRGB( float64 h, float64 s, float64 l, float32& r, float32& g, float32& b )
{
	float64 r_, g_, b_;
	ConvertHSLtoRGB( h, s, l, r_, g_, b_ );
	r = ( float32 )r_;
	g = ( float32 )g_;
	b = ( float32 )b_;
}
void ConvertHSLtoCMYK( float64 h, float64 s, float64 l, float32& c, float32& m, float32& y, float32& k )
{
	float64 r, g, b;
	ConvertHSLtoRGB( h, s, l, r, g, b );
	ConvertRGBtoCMYK( r, g, b, c, m, y, k );
}
void ConvertCMYKtoHSL( float64 c, float64 m, float64 y, float64 k, float32& h, float32& s, float32& l )
{
	float64 r, g, b;
	ConvertCMYKtoRGB( c, m, y, k, r, g, b );
	ConvertRGBtoHSL( r, g, b, h, s, l );
}
void ConvertRGBtoCMYK( const RGBColor& rgb, CMYKColor& cmyk )
{
	ConvertRGBtoCMYK( rgb.red, rgb.green, rgb.blue, cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black );
}
void ConvertCMYKtoRGB( const CMYKColor& cmyk, RGBColor& rgb )
{
	ConvertCMYKtoRGB( cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black, rgb.red, rgb.green, rgb.blue );
}
void ConvertRGBtoHSL( const RGBColor& rgb, HSLColor& hsl )
{
	ConvertRGBtoHSL( rgb.red, rgb.green, rgb.blue, hsl.hue, hsl.saturation, hsl.lightness );
}
void ConvertHSLtoRGB( const HSLColor& hsl, RGBColor& rgb )
{
	ConvertHSLtoRGB( hsl.hue, hsl.saturation, hsl.lightness, rgb.red, rgb.green, rgb.blue );
}
void ConvertHSLtoCMYK( const HSLColor& hsl, CMYKColor& cmyk )
{
	ConvertHSLtoCMYK( hsl.hue, hsl.saturation, hsl.lightness, cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black );
}
void ConvertCMYKtoHSL( const CMYKColor& cmyk, HSLColor& hsl )
{
	ConvertCMYKtoHSL( cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black, hsl.hue, hsl.saturation, hsl.lightness );
}
void ConvertRGBtoCMYK( const RGBColorF& rgb, CMYKColorF& cmyk )
{
	ConvertRGBtoCMYK( rgb.red, rgb.green, rgb.blue, cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black );
}
void ConvertCMYKtoRGB( const CMYKColorF& cmyk, RGBColorF& rgb )
{
	ConvertCMYKtoRGB( cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black, rgb.red, rgb.green, rgb.blue );
}
void ConvertRGBtoHSL( const RGBColorF& rgb, HSLColorF& hsl )
{
	ConvertRGBtoHSL( rgb.red, rgb.green, rgb.blue, hsl.hue, hsl.saturation, hsl.lightness );
}
void ConvertHSLtoRGB( const HSLColorF& hsl, RGBColorF& rgb )
{
	ConvertHSLtoRGB( hsl.hue, hsl.saturation, hsl.lightness, rgb.red, rgb.green, rgb.blue );
}
void ConvertHSLtoCMYK( const HSLColorF& hsl, CMYKColorF& cmyk )
{
	ConvertHSLtoCMYK( hsl.hue, hsl.saturation, hsl.lightness, cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black );
}
void ConvertCMYKtoHSL( const CMYKColorF& cmyk, HSLColorF& hsl )
{
	ConvertCMYKtoHSL( cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black, hsl.hue, hsl.saturation, hsl.lightness );
}
void ConvertRGBtoCMYK( const RGBColorF& rgb, CMYKColor& cmyk )
{
	ConvertRGBtoCMYK( rgb.red, rgb.green, rgb.blue, cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black );
}
void ConvertCMYKtoRGB( const CMYKColorF& cmyk, RGBColor& rgb )
{
	ConvertCMYKtoRGB( cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black, rgb.red, rgb.green, rgb.blue );
}
void ConvertRGBtoHSL( const RGBColorF& rgb, HSLColor& hsl )
{
	ConvertRGBtoHSL( rgb.red, rgb.green, rgb.blue, hsl.hue, hsl.saturation, hsl.lightness );
}
void ConvertHSLtoRGB( const HSLColorF& hsl, RGBColor& rgb )
{
	ConvertHSLtoRGB( hsl.hue, hsl.saturation, hsl.lightness, rgb.red, rgb.green, rgb.blue );
}
void ConvertHSLtoCMYK( const HSLColorF& hsl, CMYKColor& cmyk )
{
	ConvertHSLtoCMYK( hsl.hue, hsl.saturation, hsl.lightness, cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black );
}
void ConvertCMYKtoHSL( const CMYKColorF& cmyk, HSLColor& hsl )
{
	ConvertCMYKtoHSL( cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black, hsl.hue, hsl.saturation, hsl.lightness );
}
void ConvertRGBtoCMYK( const RGBColor& rgb, CMYKColorF& cmyk )
{
	ConvertRGBtoCMYK( rgb.red, rgb.green, rgb.blue, cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black );
}
void ConvertCMYKtoRGB( const CMYKColor& cmyk, RGBColorF& rgb )
{
	ConvertCMYKtoRGB( cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black, rgb.red, rgb.green, rgb.blue );
}
void ConvertRGBtoHSL( const RGBColor& rgb, HSLColorF& hsl )
{
	ConvertRGBtoHSL( rgb.red, rgb.green, rgb.blue, hsl.hue, hsl.saturation, hsl.lightness );
}
void ConvertHSLtoRGB( const HSLColor& hsl, RGBColorF& rgb )
{
	ConvertHSLtoRGB( hsl.hue, hsl.saturation, hsl.lightness, rgb.red, rgb.green, rgb.blue );
}
void ConvertHSLtoCMYK( const HSLColor& hsl, CMYKColorF& cmyk )
{
	ConvertHSLtoCMYK( hsl.hue, hsl.saturation, hsl.lightness, cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black );
}
void ConvertCMYKtoHSL( const CMYKColor& cmyk, HSLColorF& hsl )
{
	ConvertCMYKtoHSL( cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black, hsl.hue, hsl.saturation, hsl.lightness );
}
void ConvertRGBAtoCMYK( const RGBAColor& rgba, CMYKColor& cmyk )
{
	ConvertRGBtoCMYK( rgba.red, rgba.green, rgba.blue, cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black );
}
void ConvertCMYKtoRGBA( const CMYKColor& cmyk, RGBAColor& rgba )
{
	ConvertCMYKtoRGB( cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black, rgba.red, rgba.green, rgba.blue );
	rgba.alpha = 1.0;
}
void ConvertRGBAtoHSL( const RGBAColor& rgba, HSLColor& hsl )
{
	ConvertRGBtoHSL( rgba.red, rgba.green, rgba.blue, hsl.hue, hsl.saturation, hsl.lightness );
}
void ConvertHSLtoRGBA( const HSLColor& hsl, RGBAColor& rgba )
{
	ConvertHSLtoRGB( hsl.hue, hsl.saturation, hsl.lightness, rgba.red, rgba.green, rgba.blue );
	rgba.alpha = 1.0;
}
void ConvertRGBAtoCMYK( const RGBAColorF& rgba, CMYKColorF& cmyk )
{
	ConvertRGBtoCMYK( rgba.red, rgba.green, rgba.blue, cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black );
}
void ConvertCMYKtoRGBA( const CMYKColorF& cmyk, RGBAColorF& rgba )
{
	ConvertCMYKtoRGB( cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black, rgba.red, rgba.green, rgba.blue );
	rgba.alpha = 1.0f;
}
void ConvertRGBAtoHSL( const RGBAColorF& rgba, HSLColorF& hsl )
{
	ConvertRGBtoHSL( rgba.red, rgba.green, rgba.blue, hsl.hue, hsl.saturation, hsl.lightness );
}
void ConvertHSLtoRGBA( const HSLColorF& hsl, RGBAColorF& rgba )
{
	ConvertHSLtoRGB( hsl.hue, hsl.saturation, hsl.lightness, rgba.red, rgba.green, rgba.blue );
	rgba.alpha = 1.0f;
}
void ConvertRGBAtoCMYK( const RGBAColorF& rgba, CMYKColor& cmyk )
{
	ConvertRGBtoCMYK( rgba.red, rgba.green, rgba.blue, cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black );
}
void ConvertCMYKtoRGBA( const CMYKColorF& cmyk, RGBAColor& rgba )
{
	ConvertCMYKtoRGB( cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black, rgba.red, rgba.green, rgba.blue );
	rgba.alpha = 1.0;
}
void ConvertRGBAtoHSL( const RGBAColorF& rgba, HSLColor& hsl )
{
	ConvertRGBtoHSL( rgba.red, rgba.green, rgba.blue, hsl.hue, hsl.saturation, hsl.lightness );
}
void ConvertHSLtoRGBA( const HSLColorF& hsl, RGBAColor& rgba )
{
	ConvertHSLtoRGB( hsl.hue, hsl.saturation, hsl.lightness, rgba.red, rgba.green, rgba.blue );
	rgba.alpha = 1.0;
}
void ConvertRGBAtoCMYK( const RGBAColor& rgba, CMYKColorF& cmyk )
{
	ConvertRGBtoCMYK( rgba.red, rgba.green, rgba.blue, cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black );
}
void ConvertCMYKtoRGBA( const CMYKColor& cmyk, RGBAColorF& rgba )
{
	ConvertCMYKtoRGB( cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black, rgba.red, rgba.green, rgba.blue );
	rgba.alpha = 1.0f;
}
void ConvertRGBAtoHSL( const RGBAColor& rgba, HSLColorF& hsl )
{
	ConvertRGBtoHSL( rgba.red, rgba.green, rgba.blue, hsl.hue, hsl.saturation, hsl.lightness );
}
void ConvertHSLtoRGBA( const HSLColor& hsl, RGBAColorF& rgba )
{
	ConvertHSLtoRGB( hsl.hue, hsl.saturation, hsl.lightness, rgba.red, rgba.green, rgba.blue );
	rgba.alpha = 1.0f;
}
CMYKColor ConvertRGBtoCMYK( float64 r, float64 g, float64 b )
{
	CMYKColor cmyk;
	ConvertRGBtoCMYK( r, g, b, cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black );
	return cmyk;
}
RGBColor ConvertCMYKtoRGB( float64 c, float64 m, float64 y, float64 k )
{
	RGBColor rgb;
	ConvertCMYKtoRGB( c, m, y, k, rgb.red, rgb.green, rgb.blue );
	return rgb;
}
HSLColor ConvertRGBtoHSL( float64 r, float64 g, float64 b )
{
	HSLColor hsl;
	ConvertRGBtoHSL( r, g, b, hsl.hue, hsl.saturation, hsl.lightness );
	return hsl;
}
RGBColor ConvertHSLtoRGB( float64 h, float64 s, float64 l )
{
	RGBColor rgb;
	ConvertHSLtoRGB( h, s, l, rgb.red, rgb.green, rgb.blue );
	return rgb;
}
CMYKColor ConvertHSLtoCMYK( float64 h, float64 s, float64 l )
{
	CMYKColor cmyk;
	ConvertHSLtoCMYK( h, s, l, cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black );
	return cmyk;
}
HSLColor ConvertCMYKtoHSL( float64 c, float64 m, float64 y, float64 k )
{
	HSLColor hsl;
	ConvertCMYKtoHSL( c, m, y, k, hsl.hue, hsl.saturation, hsl.lightness );
	return hsl;
}
CMYKColorF ConvertRGBtoCMYK( float32 r, float32 g, float32 b )
{
	CMYKColorF cmyk;
	ConvertRGBtoCMYK( r, g, b, cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black );
	return cmyk;
}
RGBColorF ConvertCMYKtoRGB( float32 c, float32 m, float32 y, float32 k )
{
	RGBColorF rgb;
	ConvertCMYKtoRGB( c, m, y, k, rgb.red, rgb.green, rgb.blue );
	return rgb;
}
HSLColorF ConvertRGBtoHSL( float32 r, float32 g, float32 b )
{
	HSLColorF hsl;
	ConvertRGBtoHSL( r, g, b, hsl.hue, hsl.saturation, hsl.lightness );
	return hsl;
}
RGBColorF ConvertHSLtoRGB( float32 h, float32 s, float32 l )
{
	RGBColorF rgb;
	ConvertHSLtoRGB( h, s, l, rgb.red, rgb.green, rgb.blue );
	return rgb;
}
CMYKColorF ConvertHSLtoCMYK( float32 h, float32 s, float32 l )
{
	CMYKColorF cmyk;
	ConvertHSLtoCMYK( h, s, l, cmyk.cyan, cmyk.magenta, cmyk.yellow, cmyk.black );
	return cmyk;
}
HSLColorF ConvertCMYKtoHSL( float32 c, float32 m, float32 y, float32 k )
{
	HSLColorF hsl;
	ConvertCMYKtoHSL( c, m, y, k, hsl.hue, hsl.saturation, hsl.lightness );
	return hsl;
}
CMYKColor ConvertRGBtoCMYK( const RGBColor& rgb )
{
	CMYKColor cmyk;
	ConvertRGBtoCMYK( rgb, cmyk );
	return cmyk;
}
RGBColor ConvertCMYKtoRGB( const CMYKColor& cmyk )
{
	RGBColor rgb;
	ConvertCMYKtoRGB( cmyk, rgb );
	return rgb;
}
HSLColor ConvertRGBtoHSL( const RGBColor& rgb )
{
	HSLColor hsl;
	ConvertRGBtoHSL( rgb, hsl );
	return hsl;
}
RGBColor ConvertHSLtoRGB( const HSLColor& hsl )
{
	RGBColor rgb;
	ConvertHSLtoRGB( hsl, rgb );
	return rgb;
}
CMYKColor ConvertHSLtoCMYK( const HSLColor& hsl )
{
	CMYKColor cmyk;
	ConvertHSLtoCMYK( hsl, cmyk );
	return cmyk;
}
HSLColor ConvertCMYKtoHSL( const CMYKColor& cmyk )
{
	HSLColor hsl;
	ConvertCMYKtoHSL( cmyk, hsl );
	return hsl;
}
CMYKColorF ConvertRGBtoCMYK( const RGBColorF& rgb )
{
	CMYKColorF cmyk;
	ConvertRGBtoCMYK( rgb, cmyk );
	return cmyk;
}
RGBColorF ConvertCMYKtoRGB( const CMYKColorF& cmyk )
{
	RGBColorF rgb;
	ConvertCMYKtoRGB( cmyk, rgb );
	return rgb;
}
HSLColorF ConvertRGBtoHSL( const RGBColorF& rgb )
{
	HSLColorF hsl;
	ConvertRGBtoHSL( rgb, hsl );
	return hsl;
}
RGBColorF ConvertHSLtoRGB( const HSLColorF& hsl )
{
	RGBColorF rgb;
	ConvertHSLtoRGB( hsl, rgb );
	return rgb;
}
CMYKColorF ConvertHSLtoCMYK( const HSLColorF& hsl )
{
	CMYKColorF cmyk;
	ConvertHSLtoCMYK( hsl, cmyk );
	return cmyk;
}
HSLColorF ConvertCMYKtoHSL( const CMYKColorF& cmyk )
{
	HSLColorF hsl;
	ConvertCMYKtoHSL( cmyk, hsl );
	return hsl;
}
CMYKColor ConvertRGBAtoCMYK( const RGBAColor& rgba )
{
	CMYKColor cmyk;
	ConvertRGBAtoCMYK( rgba, cmyk );
	return cmyk;
}
RGBAColor ConvertCMYKtoRGBA( const CMYKColor& cmyk )
{
	RGBAColor rgba;
	ConvertCMYKtoRGBA( cmyk, rgba );
	return rgba;
}
HSLColor ConvertRGBAtoHSL( const RGBAColor& rgba )
{
	HSLColor hsl;
	ConvertRGBAtoHSL( rgba, hsl );
	return hsl;
}
RGBAColor ConvertHSLtoRGBA( const HSLColor& hsl )
{
	RGBAColor rgba;
	ConvertHSLtoRGBA( hsl, rgba );
	return rgba;
}
CMYKColorF ConvertRGBAtoCMYK( const RGBAColorF& rgba )
{
	CMYKColorF cmyk;
	ConvertRGBAtoCMYK( rgba, cmyk );
	return cmyk;
}
RGBAColorF ConvertCMYKtoRGBA( const CMYKColorF& cmyk )
{
	RGBAColorF rgba;
	ConvertCMYKtoRGBA( cmyk, rgba );
	return rgba;
}
HSLColorF ConvertRGBAtoHSL( const RGBAColorF& rgba )
{
	HSLColorF hsl;
	ConvertRGBAtoHSL( rgba, hsl );
	return hsl;
}
RGBAColorF ConvertHSLtoRGBA( const HSLColorF& hsl )
{
	RGBAColorF rgba;
	ConvertHSLtoRGBA( hsl, rgba );
	return rgba;
}
RGBColor::RGBColor( const RGBColorF& rgb ) :
	red( ( float64 )( rgb.red ) ),
	green( ( float64 )( rgb.green ) ),
	blue( ( float64 )( rgb.blue ) )
{
}
RGBAColor::RGBAColor( const RGBAColorF& rgba ) :
	red( ( float64 )( rgba.red ) ),
	green( ( float64 )( rgba.green ) ),
	blue( ( float64 )( rgba.blue ) ),
	alpha( ( float64 )( rgba.alpha ) )
{
}
CMYKColor::CMYKColor( const CMYKColorF& cmyk ) :
	cyan( ( float64 )( cmyk.cyan ) ),
	magenta( ( float64 )( cmyk.magenta ) ),
	yellow( ( float64 )( cmyk.yellow ) ),
	black( ( float64 )( cmyk.black ) )
{
}
HSLColor::HSLColor( const HSLColorF& hsl ) :
	hue( ( float64 )( hsl.hue ) ),
	saturation( ( float64 )( hsl.saturation ) ),
	lightness( ( float64 )( hsl.lightness ) )
{
}
RGBColorF::RGBColorF( const RGBColor& rgb ) :
	red( ( float32 )( rgb.red ) ),
	green( ( float32 )( rgb.green ) ),
	blue( ( float32 )( rgb.blue ) )
{
}
RGBAColorF::RGBAColorF( const RGBAColor& rgba ) :
	red( ( float32 )( rgba.red ) ),
	green( ( float32 )( rgba.green ) ),
	blue( ( float32 )( rgba.blue ) ),
	alpha( ( float32 )( rgba.alpha ) )
{
}
CMYKColorF::CMYKColorF( const CMYKColor& cmyk ) :
	cyan( ( float32 )( cmyk.cyan ) ),
	magenta( ( float32 )( cmyk.magenta ) ),
	yellow( ( float32 )( cmyk.yellow ) ),
	black( ( float32 )( cmyk.black ) )
{
}
HSLColorF::HSLColorF( const HSLColor& hsl ) :
	hue( ( float32 )( hsl.hue ) ),
	saturation( ( float32 )( hsl.saturation ) ),
	lightness( ( float32 )( hsl.lightness ) )
{
}

static Color_ostream_state curr_os_state = Color_ostream_state::OS_COLOR_RGB_3FLOAT;
std::ostream& outHexByte( std::ostream& os, uint8 _byte )
{
	static const char* const hexes = "0123456789ABCDEF";
	return os << hexes[ ( _byte >> 4 ) & 0x0fui8 ] << hexes[ _byte & 0x0fui8 ];
}
std::ostream& operator<<( std::ostream& os, const Color& rhs )
{
	switch ( curr_os_state )
	{
		case OS_COLOR_RGBA_4FLOAT:
		{
			const RGBAColor rgba = rhs;
			return os << "(r:" << rgba.red << ",g:" << rgba.green << ",b:" << rgba.blue << ",a:" << rgba.alpha << ')';
		}
		case OS_COLOR_ARGB_4FLOAT:
		{
			const RGBAColor rgba = rhs;
			return os << "(a:" << rgba.alpha << ",r:" << rgba.red << ",g:" << rgba.green << ",b:" << rgba.blue << ')';
		}
		case OS_COLOR_ARGB_4UINT:
		{
			const uint32& argbColor = rhs.m_argb;
			return os << "(a:" << ( ( argbColor & 0xff000000u ) >> 24 )
				<< ",r:" << ( ( argbColor & 0x00ff0000u ) >> 16 )
				<< ",g:" << ( ( argbColor & 0x0000ff00u ) >> 8 )
				<< ",b:" << ( argbColor & 0x000000ffu ) << ')';
		}
		case OS_COLOR_ARGB_UINT_HEXADECIMAL:
		{
			const std::ios_base::fmtflags base = os.flags() & os.basefield;
			os << "0x" << std::hex << std::setfill( '0' ) << std::setw( 8u ) << rhs.m_argb;
			if ( std::ios_base::hex != base )
			{
				if ( std::ios_base::oct == base )
					os << std::oct;
				else
					os << std::dec;
			}
			return os;
		}
		case OS_COLOR_RGB_HEXCODE:
		{
			const uint32& argbColor = rhs.m_argb;
			return outHexByte( outHexByte( outHexByte( ( os << '#' ),
				( ( unsigned char )( ( ( argbColor ) & ( 0x00ff0000u ) ) >> ( 16 ) ) ) ),
							   ( ( unsigned char )( ( ( argbColor ) & ( 0x0000ff00u ) ) >> ( 8 ) ) ) ),
							   ( ( unsigned char )( ( argbColor ) & ( 0x000000ffu ) ) ) );
		}
		case OS_COLOR_RGB_3FLOAT:
		{
			const RGBColor rgb = rhs;
			return os << "(r:" << rgb.red << ",g:" << rgb.green << ",b:" << rgb.blue << ')';
		}
		case OS_COLOR_CMYK_4FLOAT:
		{
			const CMYKColor cmyk = rhs;
			return os << "(c:" << cmyk.cyan << ",m:" << cmyk.magenta << ",y:" << cmyk.yellow << ",k:" << cmyk.black << ')';
		}
		case OS_COLOR_HSL_3FLOAT:
		{
			const HSLColor hsl = rhs;
			return os << "(h:" << hsl.hue << ",s:" << hsl.saturation << ",l:" << hsl.lightness << ')';
		}
		default:
			break;
	}
	return os;
}

std::ostream& operator<<( std::ostream& os, Color_ostream_state newState )
{
	curr_os_state = newState;
	return os;
}

bool Color::operator==( const Color& rhs ) const
{
	return m_argb == rhs.m_argb;
}
bool Color::operator!=( const Color& rhs ) const
{
	return m_argb != rhs.m_argb;
}