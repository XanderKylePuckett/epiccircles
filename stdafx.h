#pragma once
#define WIN32_LEAN_AND_MEAN
#include <utility>
#include <iostream>
#include <string>
#include <fstream>
#include <windows.h>
#include "types.h"

extern const char NULL_CHAR;
extern const float128& BAD_FLOAT128;
extern const float64& BAD_FLOAT64;
extern const float32& BAD_FLOAT32;

#define USE_SQRT_FORMULA TRUE
#define ZEROSTRUCT(X)(ZeroMemory((&(X)),(sizeof((X)))))
#define USE_DEFAULT_REL_OPS FALSE
#if USE_DEFAULT_REL_OPS
#pragma warning(disable:6326)
template <typename T>
inline bool ObjEqOp( const T& _lhs, const T& _rhs )
{
	const uint8* const pLhs_ = ( uint8* )( &_lhs );
	const uint8* const pRhs_ = ( uint8* )( &_rhs );
	if ( pLhs_ == pRhs_ )
		return true;
	static const uint32 size_ = ( uint32 )( sizeof( T ) );
	for ( uint32 i = 0u; i < size_; ++i )
		if ( pLhs_[ i ] != pRhs_[ i ] )
			return false;
	return true;
}
template <typename T>
inline bool ObjLessOp( const T& _lhs, const T& _rhs )
{
	static const uint32 size_ = ( uint32 )( sizeof( T ) );
	switch ( size_ )
	{
		case 0u:
		case 1u:
		{
			const uint8 lhs_ = ( *( ( uint8* )( &_lhs ) ) );
			const uint8 rhs_ = ( *( ( uint8* )( &_rhs ) ) );
			return ( lhs_ < rhs_ );
		}
		case 2u:
		case 3u:
		{
			const uint16 lhs_ = ( *( ( uint16* )( &_lhs ) ) );
			const uint16 rhs_ = ( *( ( uint16* )( &_rhs ) ) );
			return ( lhs_ < rhs_ );
		}
		case 4u:
		case 5u:
		case 6u:
		case 7u:
		{
			const uint32 lhs_ = ( *( ( uint32* )( &_lhs ) ) );
			const uint32 rhs_ = ( *( ( uint32* )( &_rhs ) ) );
			return ( lhs_ < rhs_ );
		}
		default:
		{
			const uint64 lhs_ = ( *( ( uint64* )( &_lhs ) ) );
			const uint64 rhs_ = ( *( ( uint64* )( &_rhs ) ) );
			return ( lhs_ < rhs_ );
		}
	}
}
#define DEFAULT_REL_OPS(XX)inline bool operator==(const XX&_lhs,const XX&_rhs){return ObjEqOp(_lhs,_rhs);}inline bool operator!=(const XX&_lhs,const XX&_rhs){return!ObjEqOp(_lhs,_rhs);}inline bool operator<(const XX&_lhs,const XX&_rhs){return ObjLessOp(_lhs,_rhs);}inline bool operator>(const XX&_lhs,const XX&_rhs){return ObjLessOp(_rhs,_lhs);}inline bool operator<=(const XX&_lhs,const XX&_rhs){return!ObjLessOp(_rhs,_lhs);}inline bool operator>=(const XX&_lhs,const XX&_rhs){return!ObjLessOp(_lhs,_rhs);}
#endif