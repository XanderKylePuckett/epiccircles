#pragma once
#pragma warning(disable:6386)

template <typename T>
class DynArray
{
private:
	T* m_arr;
	uint32 m_size, m_capacity;
	void Extend( void );
public:
	DynArray( void );
	DynArray( const DynArray<T>& );
	DynArray( const T* const, uint32 );
	DynArray& operator=( const DynArray<T>& );
	DynArray( DynArray<T>&& );
	DynArray& operator=( DynArray<T>&& );
	DynArray& operator+=( const T& );
	DynArray& operator+=( const DynArray<T>& );
	~DynArray( void );
	uint32 GetSize( void ) const;
	void Reserve( uint32 );
	uint32 AddItem( const T& );
	void RemoveItem( uint32 idx );
	void InsertItem( const T& obj, uint32 idx );
	T& GetItem( uint32 idx );
	const T& GetItem( uint32 idx ) const;
	void Clear( void );
	void DeleteArray( void );
	DynArray& OptimizeSpace( void );
	T& operator[]( uint32 );
	const T& operator[]( uint32 ) const;
	T* ExtractArray( void ) const;
	bool Find( const T&, uint32& ) const;
	bool Has( const T& ) const;
	const T* GetArr( void ) const;
	void Downsize( uint32 );
};

template <typename T>
void DynArray<T>::Extend( void )
{
	if ( m_capacity > 0u )
		Reserve( m_capacity << 1 );
	else
		Reserve( 1u );
}
template <typename T>
DynArray<T>::DynArray( void )
{
	m_arr = nullptr;
	m_size = 0u;
	m_capacity = 0u;
}
template <typename T>
DynArray<T>::DynArray( const DynArray<T>& rhs )
{
	m_capacity = rhs.m_capacity;
	m_size = rhs.m_size;
	if ( m_capacity > 0u )
	{
		m_arr = new T[ m_capacity ];
		for ( uint32 i = 0u; i < m_size; ++i )
			m_arr[ i ] = rhs.m_arr[ i ];
	}
	else
		m_arr = nullptr;
}
template <typename T>
DynArray<T>& DynArray<T>::operator=( const DynArray<T>& rhs )
{
	if ( this != &rhs )
	{
		delete[ ] m_arr;
		m_capacity = rhs.m_capacity;
		m_size = rhs.m_size;
		if ( m_capacity > 0u )
		{
			m_arr = new T[ m_capacity ];
			for ( uint32 i = 0u; i < m_size; ++i )
				m_arr[ i ] = rhs.m_arr[ i ];
		}
		else
			m_arr = nullptr;
	}
	return *this;
}
template <typename T>
DynArray<T>::DynArray( DynArray<T>&& rhs ) :
	m_arr( std::move( rhs.m_arr ) ),
	m_size( std::move( rhs.m_size ) ),
	m_capacity( std::move( rhs.m_capacity ) )
{
	rhs.m_arr = nullptr;
	rhs.m_size = 0u;
	rhs.m_capacity = 0u;
}
template <typename T>
DynArray<T>& DynArray<T>::operator=( DynArray<T>&& rhs )
{
	if ( this != &rhs )
	{
		delete[ ] m_arr;
		m_arr = std::move( rhs.m_arr );
		m_size = std::move( rhs.m_size );
		m_capacity = std::move( rhs.m_capacity );
		rhs.m_arr = nullptr;
		rhs.m_size = 0u;
		rhs.m_capacity = 0u;
	}
	return *this;
}
template <typename T>
DynArray<T>& DynArray<T>::operator+=( const T& _rhs )
{
	AddItem( _rhs );
	return *this;
}
template <typename T>
DynArray<T>& DynArray<T>::operator+=( const DynArray<T>& _rhs )
{
	Reserve( m_size + _rhs.m_size );
	for ( uint32 i = 0u; i < _rhs.m_size; ++i )
	{
		m_arr[ m_size ] = _rhs.m_arr[ i ];
		++m_size;
	}
	return *this;
}
template <typename T>
DynArray<T>::DynArray( const T* const arr, uint32 siz )
{
	if ( siz && arr )
	{
		m_capacity = m_size = siz;
		m_arr = new T[ siz ];
		for ( uint32 i = 0u; i < siz; ++i )
			m_arr[ i ] = arr[ i ];
	}
	else
	{
		m_arr = nullptr;
		m_size = 0u;
		m_capacity = 0u;
	}
}
template <typename T>
DynArray<T>::~DynArray( void )
{
	delete[ ] m_arr;
}
template <typename T>
uint32 DynArray<T>::GetSize( void ) const
{
	return m_size;
}
template <typename T>
void DynArray<T>::Reserve( uint32 cap )
{
	if ( cap > m_capacity )
	{
		m_capacity = cap;
		T* newArr = new T[ m_capacity ];
		if ( m_size > 0u )
		{
			for ( uint32 i = 0u; i < m_size; ++i )
				newArr[ i ] = m_arr[ i ];
			delete[ ] m_arr;
		}
		m_arr = newArr;
	}
}
template <typename T>
uint32 DynArray<T>::AddItem( const T& obj )
{
	if ( m_size >= m_capacity )
		Extend();
	m_arr[ m_size ] = obj;
	++m_size;
	return m_size - 1u;
}
template <typename T>
void DynArray<T>::RemoveItem( uint32 idx )
{
	if ( 0u == m_size ) return;
	if ( m_size <= idx ) return;
	--m_size;
	for ( uint32 i = idx; i < m_size; ++i )
		m_arr[ i ] = m_arr[ i + 1u ];
}
template <typename T>
void DynArray<T>::InsertItem( const T& obj, uint32 idx )
{
	if ( idx > m_size )
		idx = m_size;
	if ( m_size >= m_capacity )
		Extend();
	for ( uint32 i = m_size; idx < i; --i )
		m_arr[ i ] = m_arr[ i - 1u ];
	m_arr[ idx ] = obj;
	++m_size;
}
template <typename T>
T& DynArray<T>::GetItem( uint32 idx )
{
	if ( idx < m_size )
		return m_arr[ idx ];
	return m_arr[ m_capacity ];
}
template <typename T>
const T& DynArray<T>::GetItem( uint32 idx ) const
{
	if ( idx < m_size )
		return m_arr[ idx ];
	return m_arr[ m_capacity ];
}
template <typename T>
void DynArray<T>::Clear( void )
{
	m_size = 0u;
}
template <typename T>
void DynArray<T>::DeleteArray( void )
{
	delete[ ] m_arr;
	m_size = 0u;
	m_capacity = 0u;
}
template <typename T>
DynArray<T>& DynArray<T>::OptimizeSpace( void )
{
	if ( m_capacity > m_size )
	{
		T* newArr = ExtractArray();
		delete[ ] m_arr;
		m_capacity = m_size;
		m_arr = newArr;
	}
	return *this;
}
template <typename T>
T& DynArray<T>::operator[]( uint32 idx )
{
	if ( idx < m_size )
		return m_arr[ idx ];
	return m_arr[ m_capacity ];
}
template <typename T>
const T& DynArray<T>::operator[]( uint32 idx ) const
{
	if ( idx < m_size )
		return m_arr[ idx ];
	return m_arr[ m_capacity ];
}
template <typename T>
T* DynArray<T>::ExtractArray( void ) const
{
	if ( m_size > 0u )
	{
		T* outArr = new T[ m_size ];
		for ( uint32 i = 0u; i < m_size; ++i )
			outArr[ i ] = m_arr[ i ];
		return outArr;
	}
	return nullptr;
}
template <typename T>
bool DynArray<T>::Find( const T& obj, uint32& idx ) const
{
	for ( uint32 i = 0u; i < m_size; ++i )
	{
		if ( obj == m_arr[ i ] )
		{
			idx = i;
			return true;
		}
	}
	return false;
}
template <typename T>
bool DynArray<T>::Has( const T& obj ) const
{
	for ( uint32 i = 0u; i < m_size; ++i )
		if ( obj == m_arr[ i ] )
			return true;
	return false;
}
template <typename T>
const T* DynArray<T>::GetArr( void ) const
{
	return m_arr;
}
template <typename T>
void DynArray<T>::Downsize( uint32 _size )
{
	if ( _size < m_size )
		m_size = _size;
}