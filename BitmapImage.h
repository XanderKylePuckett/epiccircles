#pragma once
#include "Texture2D.h"

class BitmapImage
{
private:
	uint32 m_width, m_height;
	uint8* m_data;
public:
	BitmapImage( void );
	BitmapImage( const std::string& );
	BitmapImage( uint32, uint32 );
	BitmapImage( const Texture2D& );
	BitmapImage( const BitmapImage& );
	BitmapImage( BitmapImage&& );
	BitmapImage& operator=( const BitmapImage& );
	BitmapImage& operator=( BitmapImage&& );
	~BitmapImage( void );
	bool GetPixel( uint32 _x, uint32 _y, uint8& _r, uint8& _g, uint8& _b ) const;
	bool SetPixel( uint32 _x, uint32 _y, uint8 _r, uint8 _g, uint8 _b );
	bool SetPixel( uint32 _x, uint32 _y, const Color& );
	void GetSize( uint32&, uint32& ) const;
	bool SaveImage( const std::string& ) const;
	static const std::string& SaveFrame( const Texture2D&, const std::string& = "TEMP.bmp" );
};