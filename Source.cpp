#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>
#include <mutex>
#include "Raster.h"
#include "Math.h"
#include "Console.h"
std::mutex bitch_count, bitch_rand;
int countit = 0;
int main_2( int, char** );
int rand2( void )
{
	bitch_rand.lock();
	int ret = ( int )( ( std::chrono::high_resolution_clock::now().time_since_epoch().count() / ( ( long long )( rand() + 3 ) ) ) % 0x80e0LL );
	bitch_rand.unlock();
	return ret + 17;
}
void WriteIt( double x )
{
	Sleep( ( unsigned long )rand2() % 200ul );
	Console::CursorVisible( FALSE );
	Console::FlushKeys();
	do Console::SetColors( ( ConsoleColor )( rand2() % 15 + 1 ), ( ConsoleColor )( rand2() % 15 + 1 ) );
	while ( Console::GetBackground() == Console::GetForeground() );
	Console::SetCursorPosition( rand2() % Console::WindowWidth(), rand2() % 420 );
	Console::FlushKeys();
	Sleep( ( unsigned long )rand2() % 200ul );
	std::cout << std::setprecision( 9999 ) << x;
	Sleep( ( unsigned long )rand2() % 200ul );
}
double a = 9.475, b = -48.66;
void EntryDR( void )
{
	bitch_count.lock();
	++countit;
	bitch_count.unlock();
	WriteIt( a );
	WriteIt( a = 7.54 / ( b + a ) );
	WriteIt( b );
	WriteIt( b = ( 9.34 - b ) / ( ( double )rand2() / ( ( a * ( double )rand2() ) * ( double )rand2() ) ) );
	WriteIt( a = Math::Wrap01( a ) * 2.0 - 1.0 );
	WriteIt( b = Math::Wrap01( b ) * 2.0 - 1.0 );
	if ( Math::Abs( a ) < 0.00001 ) a = 0.00001;
	if ( Math::Abs( b ) < 0.00001 ) b = -0.00001;
	WriteIt( a + b );
	WriteIt( a - b );
	WriteIt( b / a );
	WriteIt( a * b );
	WriteIt( b - a );
	WriteIt( b * a );
	WriteIt( b + a );
	WriteIt( a / b );
	WriteIt( cos( b ) );
	WriteIt( sin( a ) );
	WriteIt( sin( b ) );
	WriteIt( cos( a ) );
	WriteIt( tan( a ) );
	WriteIt( tan( b ) );
	WriteIt( acos( b ) );
	WriteIt( asin( a ) );
	WriteIt( asin( b ) );
	WriteIt( acos( a ) );
	WriteIt( atan( a ) );
	WriteIt( atan2( b, a ) );
	WriteIt( atan( b ) );
	WriteIt( atan2( a, b ) );
	bitch_count.lock();
	--countit;
	bitch_count.unlock();
}
int main_0( int, char** )
{
	Raster::InitWindow( 500u, 500u );
	Texture2D& raster = Raster::GetTexture();
	uint32 x, y;
	float64 hue = 0.0;
	long long t0, t1 = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	float64 dt;
	Sleep( 5ul );
	do
	{
		t0 = t1;
		t1 = std::chrono::high_resolution_clock::now().time_since_epoch().count();
		dt = ( t1 - t0 ) * 0.000000001;
		for ( y = 0u; y < raster.GetHeight(); ++y ) for ( x = 0u; x < raster.GetWidth(); ++x )
			raster.SetPixel( x, y, Color( HSLColor( hue, ( ( float64 )y ) / raster.GetHeight(), ( ( float64 )x ) / raster.GetWidth() ) ) );
		hue += dt / 6.0;
		while ( hue >= 1.0 )
			hue -= 1.0;
	} while ( Raster::UpdateWindow() );
	Raster::ShutdownWindow();
	return EXIT_SUCCESS;
}
int main_1( int, char** )
{
	system( "pause" );
	std::cout << std::setprecision( 9999 );
	srand( ( unsigned int )std::chrono::high_resolution_clock::now().time_since_epoch().count() );
	rand(); rand();
	Console::Clear();
	for ( ;; )
	{
		bitch_count.lock();
		if ( countit < 0b0100000000000000 )
		{
			bitch_count.unlock();
			std::thread( EntryDR ).detach();
		}
		else
		{
			bitch_count.unlock();
			for ( ;; )
			{
				bitch_count.lock();
				if ( 0 == countit )
				{
					bitch_count.unlock();
					break;
				}
				bitch_count.unlock();
			}
			break;
		}
	}
	return EXIT_SUCCESS;
}
Texture2D CircleInversion( const Texture2D& og, float64 cx, float64 cy, float64 r )
{
	const uint32 w = og.GetWidth();
	const uint32 h = og.GetHeight();
	Texture2D rv( w, h, Color::BLACK );
	float64 px, py, tx, ty;
	uint32 x1, y1, x2, y2;
	r *= r;
	for ( y1 = 0u; y1 < h; ++y1 ) for ( x1 = 0u; x1 < w; ++x1 )
	{
		tx = ( float64 )x1 - cx;
		ty = ( float64 )y1 - cy;
		px = tx * r / ( tx * tx + ty * ty ) + cx;
		py = ty * r / ( tx * tx + ty * ty ) + cy;
		if ( isnan( px ) || isnan( py ) )
			continue;
		if ( isinf( px ) || isinf( py ) )
			continue;
		if ( px <= -1.0 || py <= -1.0 )
			continue;
		if ( px >= ( ( float64 )w ) || py >= ( ( float64 )h ) )
			continue;
		x2 = ( uint32 )( ( int32 )round( px ) );
		y2 = ( uint32 )( ( int32 )round( py ) );
		if ( x2 >= w || y2 >= h )
			continue;
		rv.SetPixel( x1, y1, og.GetPixel( x2, y2 ) );
	}
	return rv;
}
static const float64 pi = 3.1415926535897932384626433832795;
int main_2( int, char** )
{
	Raster::InitWindow( 500u, 500u );
	Texture2D& r = Raster::GetTexture();
	Texture2D tx = r;
	uint32 x, y;
	float64 h = 0.0, t = 0.0, dt;
	long long t0, t1 = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	Sleep( 5ul );
	do
	{
		t0 = t1;
		t1 = std::chrono::high_resolution_clock::now().time_since_epoch().count();
		dt = ( t1 - t0 ) * 0.000000001;
		for ( y = 0u; y < tx.GetHeight(); ++y ) for ( x = 0u; x < tx.GetWidth(); ++x )
			tx.SetPixel( x, y, Color( HSLColor( h, ( ( float64 )y ) / tx.GetHeight(), ( ( float64 )x ) / tx.GetWidth() ) ) );
		h += dt / 6.0;
		while ( h >= 1.0 )
			h -= 1.0;
		r = CircleInversion( tx,
							 cos( 1.3 * pi * t ) * 150.0 + 250.0,
							 sin( 0.9 * pi * t + 0.3 ) * 150.0 + 250.0,
							 sin( 2.247 * pi * t ) * 50.0 + 100.0 );
		t += dt;
	} while ( Raster::UpdateWindow() );
	Raster::ShutdownWindow();
	return EXIT_SUCCESS;
}
int main( int argc, char** argv )
{
	return main_2( argc, argv );
}