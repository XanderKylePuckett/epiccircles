#include "stdafx.h"
#include "Texture2D.h"
#include "BitmapImage.h"
#include "Math.h"
#include "Color.h"

static Color G_TRASH_COLOR = Color( 0x00000000u );
uint64 memCount = 0ull;

Texture2D::Texture2D( void ) :
	m_pixels( nullptr ), m_width( 0u ), m_height( 0u ), m_numPixels( 0u ), m_sizeLocked( false )
{
}
Texture2D::Texture2D( uint32 _squareSize, const Color& _initColor ) :
	m_width( _squareSize ), m_height( _squareSize ), m_numPixels( _squareSize * _squareSize ), m_sizeLocked( false )
{
	if ( 0u != m_numPixels )
	{
		const uint32& argb_ = _initColor.m_argb;
		++memCount;
		m_pixels = new Color[ m_numPixels ];
		for ( uint32 i = 0u; i < m_numPixels; ++i )
			m_pixels[ i ].m_argb = argb_;
	}
	else
	{
		m_width = m_height = 0u;
		m_pixels = nullptr;
	}
}
Texture2D::Texture2D( uint32 _width, uint32 _height, const Color& _initColor ) :
	m_width( _width ), m_height( _height ), m_numPixels( _width * _height ), m_sizeLocked( false )
{
	if ( 0u != m_numPixels )
	{
		const uint32& argb_ = _initColor.m_argb;
		++memCount;
		m_pixels = new Color[ m_numPixels ];
		for ( uint32 i = 0u; i < m_numPixels; ++i )
			m_pixels[ i ].m_argb = argb_;
	}
	else
	{
		m_width = m_height = 0u;
		m_pixels = nullptr;
	}
}
Texture2D::Texture2D( const Texture2D& _rhs ) :
	m_width( _rhs.m_width ), m_height( _rhs.m_height ), m_numPixels( _rhs.m_numPixels ), m_sizeLocked( false )
{
	if ( 0u != m_numPixels )
	{
		++memCount;
		m_pixels = new Color[ m_numPixels ];
		for ( uint32 i = 0u; i < m_numPixels; ++i )
			m_pixels[ i ] = _rhs.m_pixels[ i ];
	}
	else
		m_pixels = nullptr;
}
Texture2D::Texture2D( Texture2D&& _rhs ) :
	m_width( std::move( _rhs.m_width ) ),
	m_height( std::move( _rhs.m_height ) ),
	m_numPixels( std::move( _rhs.m_numPixels ) ),
	m_pixels( std::move( _rhs.m_pixels ) ), m_sizeLocked( false )
{
	if ( _rhs.m_sizeLocked )
	{
		if ( 0u != m_numPixels )
		{
			++memCount;
			m_pixels = new Color[ m_numPixels ];
			for ( uint32 i = 0u; i < m_numPixels; ++i )
				m_pixels[ i ] = _rhs.m_pixels[ i ];
		}
		else
			m_pixels = nullptr;
	}
	else
	{
		_rhs.m_width = _rhs.m_height = _rhs.m_numPixels = 0u;
		_rhs.m_pixels = nullptr;
	}
}
Texture2D& Texture2D::operator=( const Texture2D& _rhs )
{
	if ( this != &_rhs )
	{
		if ( m_sizeLocked )
		{
			*this = Color::ZERO;
			TextureToTexture( _rhs, *this );
			return *this;
		}
		if ( m_pixels ) --memCount;
		delete[ ] m_pixels;
		if ( 0u != _rhs.m_numPixels )
		{
			m_width = _rhs.m_width;
			m_height = _rhs.m_height;
			m_numPixels = _rhs.m_numPixels;
			++memCount;
			m_pixels = new Color[ m_numPixels ];
			for ( uint32 i = 0u; i < m_numPixels; ++i )
				m_pixels[ i ] = _rhs.m_pixels[ i ];
		}
		else
		{
			m_width = m_height = m_numPixels = 0u;
			m_pixels = nullptr;
		}
	}
	return *this;
}
Texture2D& Texture2D::operator=( Texture2D&& _rhs )
{
	if ( this != &_rhs )
	{
		if ( m_sizeLocked )
		{
			*this = Color::ZERO;
			TextureToTexture( _rhs, *this );
		}
		else
		{
			if ( m_pixels ) --memCount;
			delete[ ] m_pixels;
			m_pixels = std::move( _rhs.m_pixels );
			m_width = std::move( _rhs.m_width );
			m_height = std::move( _rhs.m_height );
			m_numPixels = std::move( _rhs.m_numPixels );
			if ( _rhs.m_sizeLocked )
			{
				if ( 0u != m_numPixels )
				{
					++memCount;
					m_pixels = new Color[ m_numPixels ];
					for ( uint32 i = 0u; i < m_numPixels; ++i )
						m_pixels[ i ] = _rhs.m_pixels[ i ];
				}
				else
					m_pixels = nullptr;
			}
			else
			{
				_rhs.m_width = _rhs.m_height = _rhs.m_numPixels = 0u;
				_rhs.m_pixels = nullptr;
			}
		}
	}
	return *this;
}
Texture2D& Texture2D::operator=( const Color& _rhs )
{
	const uint32& argb_ = _rhs.m_argb;
	for ( uint32 i = 0u; i < m_numPixels; ++i )
		m_pixels[ i ].m_argb = argb_;
	return *this;
}
Texture2D::~Texture2D( void )
{
	if ( m_pixels ) --memCount;
	delete[ ] m_pixels;
}
uint32 Texture2D::GetWidth( void ) const
{
	return m_width;
}
uint32 Texture2D::GetHeight( void ) const
{
	return m_height;
}
uint32 Texture2D::GetNumPixels( void ) const
{
	return m_numPixels;
}
void Texture2D::SetSize( uint32 _width, uint32 _height, const Color& _initColor )
{
	if ( m_sizeLocked )
		return;
	if ( m_pixels ) --memCount;
	delete[ ] m_pixels;
	m_numPixels = _width * _height;
	if ( 0u != m_numPixels )
	{
		const uint32& argb_ = _initColor.m_argb;
		m_height = _height;
		m_width = _width;
		++memCount;
		m_pixels = new Color[ m_numPixels ];
		for ( uint32 i = 0u; i < m_numPixels; ++i )
			m_pixels[ i ] = argb_;
	}
	else
	{
		m_width = m_height = 0u;
		m_pixels = nullptr;
	}
}
void Texture2D::SetSize_Copy( uint32 _width, uint32 _height, const Color& _fillColor )
{
	if ( m_sizeLocked )
		return;
	Texture2D old_( std::move( *this ) );
	SetSize( _width, _height, _fillColor );
	TextureToTexture( old_, *this );
}
const Color* Texture2D::GetPixels( void ) const
{
	return m_pixels;
}
void Texture2D::SetPixel( uint32 _x, uint32 _y, const Color& _color )
{
	if ( _x < m_width && _y < m_height )
		m_pixels[ _y * m_width + _x ] = _color;
}
void Texture2D::BlendPixel( uint32 _x, uint32 _y, const Color& _color )
{
	if ( _x < m_width && _y < m_height )
	{
		Color& pixel_ = m_pixels[ _y * m_width + _x ];
		pixel_ = BlendColors( pixel_, _color );
	}
}
Color Texture2D::GetPixel( uint32 _x, uint32 _y ) const
{
	if ( _x < m_width && _y < m_height )
		return m_pixels[ _y * m_width + _x ];
	return G_TRASH_COLOR = 0u;
}
const Color& Texture2D::GetPixel_Ref( uint32 _x, uint32 _y ) const
{
	if ( _x < m_width && _y < m_height )
		return m_pixels[ _y * m_width + _x ];
	return G_TRASH_COLOR = 0u;
}
Color& Texture2D::GetPixel_Ref( uint32 _x, uint32 _y )
{
	if ( _x < m_width && _y < m_height )
		return m_pixels[ _y * m_width + _x ];
	return G_TRASH_COLOR = 0u;
}
void Texture2D::TextureToTexture( const Texture2D& _from, Texture2D& _to )
{
	const uint32 maxY_ = Math::Min( _from.m_height, _to.m_height );
	const uint32 maxX_ = Math::Min( _from.m_width, _to.m_width );
	uint32 x_, y_;
	for ( y_ = 0u; y_ < maxY_; ++y_ ) for ( x_ = 0u; x_ < maxX_; ++x_ )
	{
		Color& pixel_ = _to.m_pixels[ y_ * _to.m_width + x_ ];
		pixel_ = BlendColors( pixel_, _from.m_pixels[ y_ * _from.m_width + x_ ] );
	}
}
void Texture2D::TextureToTexture( const Texture2D& _from, Texture2D& _to, uint32 _x, uint32 _y )
{
	if ( _y > _to.m_height || _x > _to.m_width ) return;
	const uint32 maxY_ = Math::Min( _from.m_height, _to.m_height - _y );
	const uint32 maxX_ = Math::Min( _from.m_width, _to.m_width - _x );
	uint32 x_, y_;
	for ( y_ = 0u; y_ < maxY_; ++y_ ) for ( x_ = 0u; x_ < maxX_; ++x_ )
	{
		Color& pixel_ = _to.m_pixels[ ( y_ + _y ) * _to.m_width + x_ + _x ];
		pixel_ = BlendColors( pixel_, _from.m_pixels[ y_ * _from.m_width + x_ ] );
	}
}
void Texture2D::SetPixelNdc( float64 _x, float64 _y, const Color& _color )
{
	uint32 xPix_, yPix_;
	NdcToScreen( _x, _y, xPix_, yPix_ );
	SetPixel( xPix_, yPix_, _color );
}
void Texture2D::NdcToScreen( float64 _inX, float64 _inY, int32& _outX, int32& _outY ) const
{
	_outX = ( ( int32 )( ( ( 0.5 ) * ( 1.0 + _inX ) ) * ( ( float64 )( m_width - 1u ) ) ) );
	_outY = ( ( int32 )( ( ( 0.5 ) * ( 1.0 - _inY ) ) * ( ( float64 )( m_height - 1u ) ) ) );
}
void Texture2D::NdcToScreen( float64 _inX, float64 _inY, uint32& _outX, uint32& _outY ) const
{
	_outX = ( ( int32 )( ( ( 0.5 ) * ( 1.0 + _inX ) ) * ( ( float64 )( m_width - 1u ) ) ) );
	_outY = ( ( int32 )( ( ( 0.5 ) * ( 1.0 - _inY ) ) * ( ( float64 )( m_height - 1u ) ) ) );
}
void Texture2D::WriteToBmp( const std::string& _filename ) const
{
	if ( nullptr == m_pixels )
		return;
	BitmapImage bitmap( m_width, m_height );
	uint32 c_, x_, y_;
	uint8 r_, g_, b_;
	for ( y_ = 0u; y_ < m_height; ++y_ ) for ( x_ = 0u; x_ < m_width; ++x_ )
	{
		c_ = m_pixels[ y_ * m_width + x_ ].m_argb;
		r_ = ( uint8 )( ( c_ & 0x00ff0000u ) >> 16 );
		g_ = ( uint8 )( ( c_ & 0x0000ff00u ) >> 8 );
		b_ = ( uint8 )( c_ & 0x000000ffu );
		bitmap.SetPixel( x_, y_, r_, g_, b_ );
	}
	bitmap.SaveImage( _filename );
}
Texture2D Texture2D::GetTexture2DFromBmp( const std::string& _filename )
{
	BitmapImage bitmap_ = BitmapImage( std::string( _filename ) );
	uint32 width_, height_;
	bitmap_.GetSize( width_, height_ );
	if ( 0u == width_ || 0u == height_ )
		return Texture2D();
	Texture2D outTexture_( width_, height_ );
	uint32 x_, y_;
	uint8 r_, g_, b_;
	for ( y_ = 0u; y_ < height_; ++y_ ) for ( x_ = 0u; x_ < width_; ++x_ )
	{
		bitmap_.GetPixel( x_, y_, r_, g_, b_ );
		outTexture_.m_pixels[ y_ * width_ + x_ ].m_argb =
			0xff000000u | ( ( ( uint32 )( r_ ) ) << 16u ) |
			( ( ( uint32 )( g_ ) ) << 8u ) | ( ( uint32 )( b_ ) );
	}
	return outTexture_;
}
Texture2D::Texture2D( const std::string& _bmpfile ) :
	Texture2D( std::move( GetTexture2DFromBmp( _bmpfile ) ) )
{
}