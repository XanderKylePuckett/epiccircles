#include "stdafx.h"
#include "BitmapImage.h"

BitmapImage::BitmapImage( void ) :
	m_width( 0u ),
	m_height( 0u ),
	m_data( nullptr )
{
}
BitmapImage::BitmapImage( const std::string& _filename ) :
	m_width( 0u ),
	m_height( 0u ),
	m_data( nullptr )
{
	std::ifstream file_;
	file_.open( _filename, std::ios_base::in | std::ios_base::binary );
	if ( !file_.is_open() )
		return;
	file_.seekg( 0ll, std::ios::end );
	const uint32 fileSize_ = ( uint32 )( file_.tellg() );
	file_.close();
	file_.open( _filename, std::ios_base::in | std::ios_base::binary );
	if ( !file_.is_open() )
		return;
	uint32 tmp32_;
	uint16& tmp16_ = *( ( uint16* )( &tmp32_ ) );
	static int8 nothing_[ 24u ];
	file_.read( ( char* )( &tmp16_ ), 2u );
	if ( 19778ui16 != tmp16_ )
	{
		file_.close();
		return;
	};
	file_.read( nothing_, 6u );
	file_.read( ( char* )( &tmp32_ ), 4u );
	if ( 40u != tmp32_ )
	{
		tmp32_ >>= 16u;
		if ( 54u != tmp32_ )
		{
			file_.close();
			return;
		}
		file_.read( nothing_, 6u );
	}
	file_.read( ( char* )( &m_width ), 4u );
	file_.read( ( char* )( &m_height ), 4u );
	if ( 0u == m_width || 0u == m_height )
	{
		file_.close();
		m_width = m_height = 0u;
		return;
	}
	const uint32 rowInc_ = 3u * m_width;
	const uint32 padding_ = ( 4u - ( rowInc_ & 3u ) ) & 3u;
	if ( fileSize_ != m_height * ( rowInc_ + padding_ ) + 54u )
	{
		file_.close();
		m_width = m_height = 0u;
		return;
	}
	file_.read( nothing_, 2u );
	file_.read( ( char* )( &tmp16_ ), 2u );
	if ( 24ui16 != tmp16_ )
	{
		file_.close();
		m_width = m_height = 0u;
		return;
	}
	file_.read( nothing_, 24u );
	m_data = new uint8[ rowInc_ * m_height ];
	for ( uint32 i = 0u; i < m_height; ++i )
	{
		file_.read( ( char* )( &m_data[ rowInc_ * ( m_height - i - 1u ) ] ), rowInc_ );
		file_.read( nothing_, padding_ );
	}
	file_.close();
}
BitmapImage::BitmapImage( uint32 _width, uint32 _height ) :
	m_width( _width ),
	m_height( _height )
{
	if ( 0u == m_width || 0u == m_height )
	{
		m_width = m_height = 0u;
		m_data = nullptr;
	}
	else
		m_data = new uint8[ 3u * m_width * m_height ];
}
BitmapImage::BitmapImage( const Texture2D& _texture ) :
	BitmapImage( _texture.m_width, _texture.m_height )
{
	if ( nullptr == m_data )
		return;
	uint32 y_, x_, offset_, color_;
	for ( y_ = 0u; y_ < m_height; ++y_ ) for ( x_ = 0u; x_ < m_width; ++x_ )
	{
		color_ = _texture.m_pixels[ offset_ = y_ * m_width + x_ ].m_argb;
		m_data[ offset_ += ( offset_ << 1u ) ] = ( uint8 )( color_ );
		m_data[ offset_ + 1u ] = ( uint8 )( color_ >> 8u );
		m_data[ offset_ + 2u ] = ( uint8 )( color_ >> 16u );
	}
}
BitmapImage::BitmapImage( const BitmapImage& _rhs ) :
	m_width( _rhs.m_width ),
	m_height( _rhs.m_height )
{
	if ( nullptr != _rhs.m_data )
	{
		const uint32 arrSize_ = 3u * m_width * m_height;
		m_data = new uint8[ arrSize_ ];
		for ( uint32 i = 0u; i < arrSize_; ++i )
			m_data[ i ] = _rhs.m_data[ i ];
	}
	else
		m_data = nullptr;
}
BitmapImage::BitmapImage( BitmapImage&& _rhs ) :
	m_width( std::move( _rhs.m_width ) ),
	m_height( std::move( _rhs.m_height ) ),
	m_data( std::move( _rhs.m_data ) )
{
	_rhs.m_data = nullptr;
}
BitmapImage& BitmapImage::operator=( const BitmapImage& _rhs )
{
	if ( this != &_rhs )
	{
		delete[ ] m_data;
		m_width = _rhs.m_width;
		m_height = _rhs.m_height;
		if ( nullptr != _rhs.m_data )
		{
			const uint32 arrSize_ = 3u * m_width * m_height;
			m_data = new uint8[ arrSize_ ];
			for ( uint32 i = 0u; i < arrSize_; ++i )
				m_data[ i ] = _rhs.m_data[ i ];
		}
		else
			m_data = nullptr;
	}
	return *this;
}
BitmapImage& BitmapImage::operator=( BitmapImage&& _rhs )
{
	if ( this != &_rhs )
	{
		delete[ ] m_data;
		m_width = std::move( _rhs.m_width );
		m_height = std::move( _rhs.m_height );
		m_data = std::move( _rhs.m_data );
		_rhs.m_data = nullptr;
	}
	return *this;
}
BitmapImage::~BitmapImage( void )
{
	delete[ ] m_data;
}
bool BitmapImage::GetPixel( uint32 _x, uint32 _y, uint8& _r, uint8& _g, uint8& _b ) const
{
	if ( _x >= m_width || _y >= m_height )
		return false;
	const uint32 offset = 3u * ( _y * m_width + _x );
	_b = m_data[ offset ];
	_g = m_data[ offset + 1u ];
	_r = m_data[ offset + 2u ];
	return true;
}
bool BitmapImage::SetPixel( uint32 _x, uint32 _y, uint8 _r, uint8 _g, uint8 _b )
{
	if ( _x >= m_width || _y >= m_height )
		return false;
	const uint32 offset_ = 3u * ( _y * m_width + _x );
	m_data[ offset_ ] = _b;
	m_data[ offset_ + 1u ] = _g;
	m_data[ offset_ + 2u ] = _r;
	return true;
}
bool BitmapImage::SetPixel( uint32 _x, uint32 _y, const Color& _color )
{
	return SetPixel(
		_x, _y,
		( uint8 )( _color.m_argb >> 16 ),
		( uint8 )( _color.m_argb >> 8 ),
		( uint8 )( _color.m_argb ) );
}
const std::string& BitmapImage::SaveFrame( const Texture2D& _texture, const std::string& _filename )
{
	BitmapImage( _texture ).SaveImage( _filename );
	return _filename;
}
void BitmapImage::GetSize( uint32& _width, uint32& _height ) const
{
	_width = m_width;
	_height = m_height;
}
bool BitmapImage::SaveImage( const std::string& _filename ) const
{
	if ( nullptr == m_data )
		return false;
	std::ofstream file_;
	file_.open( _filename, std::ios_base::binary | std::ios_base::out | std::ios_base::trunc );
	if ( file_.is_open() )
	{
		const uint32 rowInc_ = 3u * m_width;
		const uint32 sizeImage_ = ( ( 3u + rowInc_ ) & 0x0000fffcu ) * m_height;
		static const int8 zeroes_[ 16u ] = { 0i8 };
		uint16 tmp16_ = 19778ui16;
		uint32 tmp32_ = sizeImage_ + 54u;
		file_.write( ( char* )( &tmp16_ ), 2u );
		file_.write( ( char* )( &tmp32_ ), 4u );
		file_.write( zeroes_, 4u );
		file_.write( ( char* )( &( tmp32_ = 54u ) ), 4u );
		file_.write( ( char* )( &( tmp32_ = 40u ) ), 4u );
		file_.write( ( char* )( &m_width ), 4u );
		file_.write( ( char* )( &m_height ), 4u );
		file_.write( ( char* )( &( tmp16_ = 1ui16 ) ), 2u );
		file_.write( ( char* )( &( tmp16_ = 24ui16 ) ), 2u );
		file_.write( zeroes_, 4u );
		file_.write( ( const char* )( &sizeImage_ ), 4u );
		file_.write( zeroes_, 16u );
		const uint32 padding_ = ( 4u - ( rowInc_ & 3u ) ) & 3u;
		for ( uint32 i = 0u; i < m_height; ++i )
		{
			file_.write( ( char* )( &m_data[ ( rowInc_ * ( m_height - i - 1u ) ) ] ), rowInc_ );
			file_.write( zeroes_, padding_ );
		}
		file_.close();
		return true;
	}
	return false;
}