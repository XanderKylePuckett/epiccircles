#include "stdafx.h"

const char NULL_CHAR = '\0';
const uint128 uint128::ZERO = uint128( 0ui64, 0ui64 );
const uint128 uint128::ONE = uint128( 0ui64, 1ui64 );
static const uint128 BAD_FLOAT_BITS = ( ~( uint128::ZERO ) );
const float128& BAD_FLOAT128 = *( ( float128* )( &BAD_FLOAT_BITS ) );
const float64& BAD_FLOAT64 = *( ( float64* )( &BAD_FLOAT_BITS ) );
const float32& BAD_FLOAT32 = *( ( float32* )( &BAD_FLOAT_BITS ) );