#include "stdafx.h"
#include "String.h"

static const DynArray<char> nullStr = DynArray<char>( &NULL_CHAR, 1u );
static char trashChar = NULL_CHAR;

String::String( void ) :
	m_str( &NULL_CHAR, 1u )
{
}
String::String( const String& _rhs ) :
	m_str( _rhs.m_str )
{
}
String::String( String&& _rhs ) :
	m_str( std::move( _rhs.m_str ) )
{
}
String::String( const char* const _rhs )
{
	if ( nullptr != _rhs )
	{
		uint32 i = 0u;
		while ( NULL_CHAR != _rhs[ i ] )
		{
			m_str.AddItem( _rhs[ i ] );
			++i;
		}
		m_str.AddItem( NULL_CHAR );
		m_str.OptimizeSpace();
	}
	else m_str = nullStr;
}
String::String( char _rhs )
{
	m_str.AddItem( _rhs );
	m_str.AddItem( NULL_CHAR );
}
String& String::operator=( const String& _rhs )
{
	if ( this != &_rhs )
	{
		m_str = _rhs.m_str;
	}
	return *this;
}
String& String::operator=( String&& _rhs )
{
	if ( this != &_rhs )
	{
		m_str = std::move( _rhs.m_str );
	}
	return *this;
}
String::~String( void )
{
}
String String::operator+( const String& _rhs ) const
{
	String outString_;
	outString_.m_str.Clear();
	uint32 i_ = 0u, len_ = GetLength();
	for ( ; i_ < len_; ++i_ )
		outString_.m_str.AddItem( m_str[ i_ ] );
	len_ = _rhs.GetLength();
	for ( i_ = 0u; i_ < len_; ++i_ )
		outString_.m_str.AddItem( _rhs.m_str[ i_ ] );
	if ( 0u == outString_.m_str.GetSize() )
		outString_.m_str = nullStr;
	return outString_.OptimizeSpace();
}
String& String::operator+=( const String& _rhs )
{
	return ( *this = *this + _rhs ).OptimizeSpace();
}
String String::operator*( uint32 _rhs ) const
{
	String outString_;
	for ( uint32 i = 0u; i < _rhs; ++i )
		outString_ += *this;
	return outString_.OptimizeSpace();

}
String operator*( uint32 _lhs, const String& _rhs )
{
	return _rhs * _lhs;
}
String& String::operator*=( uint32 _rhs )
{
	return ( *this = *this * _rhs ).OptimizeSpace();
}
const char& String::operator[]( uint32 _idx ) const
{
	if ( _idx < m_str.GetSize() )
		return m_str[ _idx ];
	return NULL_CHAR;
}
char& String::operator[]( uint32 _idx )
{
	if ( _idx == m_str.GetSize() - 1u )
	{
		OptimizeSpace();
		m_str.Reserve( m_str.GetSize() + 1u );
		m_str.AddItem( NULL_CHAR );
	}
	if ( _idx < m_str.GetSize() )
		return m_str[ _idx ];
	return trashChar = NULL_CHAR;
}
const char* const String::GetString( void ) const
{
	return m_str.GetArr();
}
std::ostream& operator<<( std::ostream& _os, const String& _rhs )
{
	return _os << _rhs.m_str.GetArr();
}
uint32 String::GetLength( void ) const
{
	for ( uint32 i = 0u; i < m_str.GetSize(); ++i )
		if ( NULL_CHAR == m_str[ i ] )
			return i;
	return 0u;
}
String& String::OptimizeSpace( void )
{
	for ( uint32 i = 0u; i < m_str.GetSize(); ++i )
		if ( NULL_CHAR == m_str[ i ] )
			m_str.Downsize( i + 1u );
	m_str.OptimizeSpace();
	return *this;
}
bool String::operator==( const String& _rhs ) const
{
	const String lhs_ = String( *this ).OptimizeSpace();
	const String rhs_ = String( _rhs ).OptimizeSpace();
	const uint32 len_ = lhs_.GetLength();
	if ( len_ != rhs_.GetLength() )
		return false;
	for ( uint32 i = 0u; i < len_; ++i )
		if ( lhs_[ i ] != rhs_[ i ] )
			return false;
	return true;
}
bool String::operator!=( const String& _rhs ) const
{
	const String lhs_ = String( *this ).OptimizeSpace();
	const String rhs_ = String( _rhs ).OptimizeSpace();
	const uint32 len_ = lhs_.GetLength();
	if ( len_ != rhs_.GetLength() )
		return true;
	for ( uint32 i = 0u; i < len_; ++i )
		if ( lhs_[ i ] != rhs_[ i ] )
			return true;
	return false;
}
bool String::operator<( const String& _rhs ) const
{
	const String lhs_ = String( *this ).OptimizeSpace();
	const String rhs_ = String( _rhs ).OptimizeSpace();
	// TODO
	return false;
}
bool String::operator<=( const String& _rhs ) const
{
	const String lhs_ = String( *this ).OptimizeSpace();
	const String rhs_ = String( _rhs ).OptimizeSpace();
	// TODO
	return true;
}
bool String::operator>( const String& _rhs ) const
{
	const String lhs_ = String( *this ).OptimizeSpace();
	const String rhs_ = String( _rhs ).OptimizeSpace();
	// TODO
	return false;
}
bool String::operator>=( const String& _rhs ) const
{
	const String lhs_ = String( *this ).OptimizeSpace();
	const String rhs_ = String( _rhs ).OptimizeSpace();
	// TODO
	return true;
}